<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sqlMap PUBLIC "-//iBATIS.com//DTD SQL Map 2.0//EN" "http://www.ibatis.com/dtd/sql-map-2.dtd">

<sqlMap namespace="Logging">
	
	<insert id="logBibTexURL" parameterClass="bibtexExtraParam">
		INSERT INTO log_bibtexurls (content_id, url, text, `group`, date)
		SELECT content_id, url, text, `group`, date
		FROM bibtexurls WHERE content_id = #requestedContentId# AND url = #bibtexExtra.url#
	</insert>
	
	<insert id="logBibTexURLs" parameterClass="bibtexParam">
		INSERT INTO log_bibtexurls (content_id, url, text, `group`, date)
		SELECT content_id, url, text, `group`, date
		FROM bibtexurls WHERE content_id = #requestedContentId#
	</insert>

	<!-- FIXME: Dublicate Entry exceptions occure during batch editing without apparent reason. Since only the logging is erroneous the IGNORE insertion is the current hotfix -->
	<!-- INSERT IGNORE INTO log_bibtex (title, entrytype, author, editor, year, content_id, journal, volume, chapter, -->
	<insert id="logBibTex" parameterClass="bibtexParam">
		INSERT INTO log_bibtex (title, entrytype, author, editor, year, content_id, journal, volume, chapter,
		                        edition, month, bookTitle, howpublished, institution, organization, publisher,
		                        address, school, series, bibtexKey, `group`, date, change_date, user_name, url, description,
		                        annote, note, pages, bKey, number, crossref, misc, bibtexAbstract, type, day,
		                        scraperid, privnote, simhash0, simhash1, simhash2, simhash3, new_content_id, current_content_id)
		SELECT title, entrytype, author, editor, year, content_id, journal, volume, chapter, edition, month,
		       bookTitle, howpublished, institution, organization, publisher, address, school, series, bibtexKey,
		       `group`, date, change_date, user_name, url, description, annote, note, pages, bKey, number, crossref, misc,
		       bibtexAbstract, type, day, scraperid, privnote, simhash0, simhash1, simhash2, simhash3, #newContentId#, #newContentId#
		FROM bibtex WHERE content_id = #requestedContentId#
	</insert>
	
	<insert id="logPublicationMassUpdate" parameterClass="bibtexParam">
		INSERT INTO log_bibtex (title, entrytype, author, editor, year, content_id, journal, volume, chapter,
		                        edition, month, bookTitle, howpublished, institution, organization, publisher,
		                        address, school, series, bibtexKey, `group`, date, change_date, user_name, url, description,
		                        annote, note, pages, bKey, number, crossref, misc, bibtexAbstract, type, day,
		                        scraperid, privnote, simhash0, simhash1, simhash2, simhash3, new_content_id, current_content_id)
		SELECT title, entrytype, author, editor, year, content_id, journal, volume, chapter, edition, month,
		       bookTitle, howpublished, institution, organization, publisher, address, school, series, bibtexKey,
		       `group`, date, change_date, user_name, url, description, annote, note, pages, bKey, number, crossref, misc,
		       bibtexAbstract, type, day, scraperid, privnote, simhash0, simhash1, simhash2, simhash3, content_id, content_id
		FROM bibtex WHERE user_name = #requestedUserName# AND `group` = #groupId#
	</insert>
	
	<insert id="logGoldStandard">
		INSERT INTO log_gold_standard (new_simhash1, content_id, new_content_id, current_content_id, change_date, <include refid ="allGoldStandardPublicationColumns"/>)
		SELECT #newHash#, content_id, #newContentId#, #newContentId#, change_date, <include refid ="allGoldStandardPublicationColumns"/>
		FROM gold_standard WHERE simhash1 = #oldHash#
	</insert>
		
	<insert id="logGoldStandardRelationDelete" parameterClass="goldstandardReferenceParam">
		INSERT INTO log_gold_standard_relations (<include refid="commonLogGoldStandardReferenceAttributes" />) VALUES (#hash#, #refHash#, #username#, #relation#)
	</insert>

	<insert id="logBookmark" parameterClass="bookmarkParam">
		INSERT INTO log_bookmark (content_id, book_url_hash, book_description, book_extended, `group`, date, change_date, user_name, new_content_id, current_content_id)
		SELECT content_id, book_url_hash, book_description, book_extended, `group`, date, change_date, user_name, #newContentId#, #newContentId#
		FROM bookmark WHERE content_id = #requestedContentId#
	</insert>

	<update id="logBookmarkUpdate" parameterClass="bookmarkParam">
		UPDATE log_bookmark SET new_content_id = #newContentId# WHERE content_id = #requestedContentId#
	</update>
	
	<insert id="logBookmarkMassUpdate" parameterClass="bookmarkParam">
		INSERT INTO log_bookmark (content_id, book_url_hash, book_description, book_extended, `group`, date, change_date, user_name, new_content_id, current_content_id)
		SELECT content_id, book_url_hash, book_description, book_extended, `group`, date, change_date, user_name, content_id, content_id
		FROM bookmark WHERE user_name = #requestedUserName# AND `group` = #groupId#
	</insert>
	
	<insert id="logTagRelation" parameterClass="tagRelationParam">	
		INSERT INTO log_tagtagrelations (relationID, lower, upper, date_of_create, user_name) 
		SELECT relationID, lower, upper, date_of_create, user_name
		FROM tagtagrelations WHERE lower=#lowerTagName# AND upper=#upperTagName# AND user_name=#ownerUserName#
	</insert>

	<insert id="logConcept" parameterClass="tagRelationParam">	
		INSERT INTO log_tagtagrelations (relationID, lower, upper, date_of_create, user_name) 
		SELECT relationID, lower, upper, date_of_create, user_name
		FROM tagtagrelations WHERE upper=#upperTagName# AND user_name=#ownerUserName#
	</insert>

	<insert id="logTasDelete" parameterClass="tagParam">
		INSERT INTO log_tas (tas_id, tag_name, content_id, content_type, date, change_date)
		SELECT tas_id, tag_name, content_id, content_type, date,change_date
		FROM tas WHERE content_id = #requestedContentId#
	</insert>

	<insert id="logChangeUserMembershipInGroup" parameterClass="groupParam">
		INSERT INTO log_group_memberships (`user_name`, `group`, `defaultgroup`, `start_date`, `group_role`, `user_shared_documents`)
		SELECT g.user_name, g.group, g.defaultgroup, g.start_date, g.group_role, g.user_shared_documents
		FROM group_memberships g WHERE g.user_name = #userName# AND g.group = #groupId#
	</insert>
	
	<insert id="logClipboardItemDelete" parameterClass="clipboardParam">
		INSERT INTO log_collector (user_name, content_id, add_date)
		SELECT c.user_name, c.content_id, c.date FROM collector c, bibtex b
		WHERE c.content_id = b.content_id
		AND b.simhash$simHash$ = #hash#
		AND b.user_name = #requestedUserName# AND c.user_name = #userName#
	</insert>
	
	<insert id="logDeleteAllFromClipboard" parameterClass="String">
		INSERT INTO log_collector (user_name, content_id, add_date)
		SELECT c.user_name, c.content_id, c.date 
			FROM collector c
			WHERE c.user_name = #userName#;	
	</insert>

	<insert id="logUser" parameterClass="String">
		INSERT INTO log_user (	user_name,  user_email, user_password, user_password_salt, user_homepage, user_realname, spammer,
								openurl, reg_date, ip_address, tmp_password, tmp_request_date, tagbox_style, 
								tagbox_sort, tagbox_minfreq, tagbox_max_count, is_max_count, tagbox_tooltip, list_itemcount, spammer_suggest,
								birthday, gender, profession, institution, interests, hobbies, place,  profilegroup, api_key, 
								updated_by, updated_at, lang, role, timestamp, to_classify, show_bookmark, show_bibtex, simple_interface, useExternalPicture)
		(SELECT user_name, user_email, user_password, user_password_salt, user_homepage, user_realname, spammer,
				openurl, reg_date, ip_address, tmp_password, tmp_request_date, tagbox_style,
				tagbox_sort, tagbox_minfreq, tagbox_max_count, is_max_count, tagbox_tooltip, list_itemcount, spammer_suggest,
				birthday, gender, profession, institution, interests, hobbies, place, profilegroup, api_key, 
				updated_by, updated_at, lang, role, UNIX_TIMESTAMP(NOW()), to_classify, show_bookmark, show_bibtex, simple_interface, useExternalPicture
		 FROM user WHERE user_name = #userName#) 
	</insert>
	
	<!-- TODO: merge with logFriendsDelete -->
	<insert id="logFollowerDelete" parameterClass="userParam">
		INSERT INTO log_friends (friends_id, user_name, f_user_name, tag_name, friendship_date)
			SELECT friends_id, user_name, f_user_name, tag_name, friendship_date FROM friends
			WHERE tag_name = #followerTag# AND user_name = #userName# AND f_user_name = #requestedUserName#
	</insert>
	
	<insert id="logFriendDelete" parameterClass="userParam">
		INSERT INTO log_friends (friends_id, user_name, f_user_name, tag_name, friendship_date) 
			SELECT friends_id, user_name, f_user_name, tag_name, friendship_date FROM friends 
			WHERE user_name = #userName# AND f_user_name = #requestedUserName# AND tag_name = #tag.name#
	</insert>
	
	<!-- spam framework logs -->
	
	<insert id="logPrediction" parameterClass="adminParam">
		INSERT IGNORE INTO log_prediction (`user_name`, `prediction`, `timestamp`, `updated_at`, `algorithm` ,`mode`, `confidence`)
		VALUES (#userName#, #prediction#, UNIX_TIMESTAMP(NOW()), #updatedAt#, #algorithm#, #mode#, #confidence#)
	</insert>

	<insert id="logCurrentPrediction" parameterClass="adminParam">
		REPLACE INTO prediction (`user_name`, `prediction`, `timestamp`, `updated_at`, `algorithm` , `mode`, `evaluator`, `confidence`)
		VALUES (#userName#, #prediction#, UNIX_TIMESTAMP(NOW()), #updatedAt#, #algorithm#, #mode#, 0, #confidence#)
	</insert>
	
	<!-- discussion logs -->
	
	<insert id="logDiscussionItem" parameterClass="int">
		INSERT INTO log_discussion (<include refid ="discussionColumns"/>)
			SELECT <include refid ="commonDiscussionLogsAttributes"/>
				FROM discussion d
			WHERE d.discussion_id = #id#
	</insert>
	
	<insert id="logDiscussionMassUpdate" parameterClass="discussionItemParam">
		INSERT INTO log_discussion (<include refid ="discussionColumns"/>)
			SELECT <include refid ="commonDiscussionLogsAttributes"/>
				FROM discussion d
			WHERE d.user_name=#userName# AND `group`=#groupId#
	</insert>

	<!-- document logs -->
	<insert id="logDocument" parameterClass="documentParam">
		INSERT INTO log_document (hash, content_id, name, user_name, date, md5hash, log_date)
		  VALUES (#fileHash#, #contentId#, #fileName#, #userName#, #date#, #md5hash#, NOW())
	</insert>
	
	<!-- inbox logs -->
	<insert id="logInboxMessages" parameterClass="inboxParam">
		INSERT INTO log_inboxMail (message_id, content_id, intraHash, sender_user, receiver_user, date, content_type, log_date)
 		  SELECT message_id, content_id, intraHash, sender_user, receiver_user, date, content_type, NOW() as log_date 
 		  FROM inboxMail WHERE 
 		  <isGreaterThan property="messageId" compareValue="0">
 		  	inboxMail.message_id = #messageId#
 		  </isGreaterThan>
 		  <isLessEqual property="messageId" compareValue="0">
 		  	inboxMail.receiver_user = #receiver#;
 		  </isLessEqual> 
	</insert>
	
	<insert id="logPersonName" parameterClass="loggingParam">
		INSERT INTO log_person_name (person_change_id, first_name, last_name, person_id, is_main, log_changed_at, log_changed_by, log_date, edited_by)
		SELECT person_change_id, first_name, last_name, person_id, is_main, log_changed_at, log_changed_by, #date#, #postEditor.name#
		FROM person_name
			WHERE person_change_id = #oldContentId#
	</insert>

	<insert id="logPubPerson" parameterClass="loggingParam">
		INSERT INTO log_pub_person (person_change_id, simhash1, simhash2, relator_code, person_index, person_id, qualifying, log_changed_at, log_changed_by, log_date, edited_by <isNotNull property="newContentId">, new_change_id</isNotNull> )
		SELECT person_change_id, simhash1, simhash2, relator_code, person_index, person_id, qualifying, log_changed_at, log_changed_by, #date#, #postEditor.name# <isNotNull property="newContentId">, #newContentId#</isNotNull>
		FROM pub_person
			WHERE person_change_id = #oldContentId#
	</insert>

	<insert id="logPersonNames" parameterClass="loggingParam">
		INSERT INTO log_person_name (person_change_id, first_name, last_name, person_id, is_main, log_changed_at, log_changed_by, log_date, edited_by)
		SELECT person_change_id, first_name, last_name, person_id, is_main, log_changed_at, log_changed_by, #date#, #postEditor.name#
		FROM person_name
		WHERE person_id = #oldHash#
	</insert>
	
	<!-- when updated, 'deleted' field is 0 -->
	<insert id="logPersonUpdate" parameterClass="String">
		INSERT INTO log_person (person_change_id, person_id, academic_degree, user_name, orcid, college, email, homepage, log_changed_at, log_changed_by, deleted)
		SELECT person_change_id, person_id, academic_degree, user_name, orcid, college, email, homepage, log_changed_at, log_changed_by, 0
		FROM person WHERE person_id = #personId#
	</insert>
	
	<!-- when updated, 'deleted' field is 0 -->
	<insert id="logPersonUpdateByUserName" parameterClass="String">
		INSERT INTO log_person (person_change_id, person_id, academic_degree, user_name, orcid, college, email, homepage, log_changed_at, log_changed_by, deleted)
		SELECT person_change_id, person_id, academic_degree, user_name, orcid, college, email, homepage, log_changed_at, log_changed_by, 0
		FROM person WHERE user_name = #username#
	</insert>
	
	<!-- when deleted, 'deleted' field is 1 -->
	<insert id="logPersonDelete" parameterClass="person">
		INSERT INTO log_person (person_change_id, person_id, academic_degree, user_name, orcid, college, email, homepage, log_changed_at, log_changed_by, deleted)
		SELECT person_change_id, person_id, academic_degree, user_name, orcid, college, email, homepage,  #changeDate#, #changedBy#, 1 as deleted
		FROM person WHERE person_id = #personId#
	</insert>

	<insert id="logPostInsert" parameterClass="loggingParam">
		INSERT INTO log_postchange VALUES (#postOwner.name#, #postEditor.name#, #oldContentId#, #newContentId#, #newContentId#, #contentType#, #date#)
	</insert>
	
	<update id="logPostUpdateCurrentID" parameterClass="loggingParam">
		UPDATE log_postchange
		SET current_content_id = #newContentId#
		WHERE current_content_id = #oldContentId#
	</update>

	<insert id="logProjectUpdate" parameterClass="loggingParam">
		INSERT INTO log_projects (<include refid="allProjectAttributes" />, new_id, log_date, log_user)
		SELECT <include refid="allProjectAttributes" />, #newContentId#, #date#, #postEditor.name#
		FROM projects
		WHERE id = #oldContentId#
	</insert>

	<insert id="logCRISLinkUpdate" parameterClass="loggingParam">
		INSERT INTO log_cris_links (<include refid="allCRISLinkAttributesLog" />, new_id, log_date, log_user)
		SELECT <include refid="allCRISLinkAttributes" />, #newContentId#, #date#, #postEditor.name#
		FROM cris_links c
		WHERE c.id = #oldContentId#
	</insert>

	<insert id="logGroup" parameterClass="org.bibsonomy.database.params.logging.InsertGroupLog">
		INSERT INTO log_groupids (group_name, `group`, privlevel, sharedDocuments, allow_join, shortDescription, organization, internal_id, log_reason, log_date, log_user)
		SELECT group_name, `group`, privlevel, sharedDocuments, allow_join, shortDescription, organization, internal_id, #logReason#, #loggedTimestamp#, #loggedUser.name#
		FROM groupids
		WHERE `group` = #group.groupId#
	</insert>

	<insert id="logGroupUpdate" parameterClass="org.bibsonomy.database.params.logging.InsertUserGroupLog">
		INSERT INTO log_groupids (group_name, `group`, privlevel, sharedDocuments, allow_join, shortDescription, organization, internal_id, log_reason, log_date, log_user)
		SELECT group_name, `group`, privlevel, sharedDocuments, allow_join, shortDescription, organization, internal_id, #logReason#, #loggedTimestamp#, #loggedUser.name#
		FROM groupids
		WHERE group_name = #userName#
	</insert>

</sqlMap>
