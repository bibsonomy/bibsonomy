/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper.url.kde.springer;

import static org.bibsonomy.scraper.junit.RemoteTestAssert.assertScraperResult;
import org.bibsonomy.junit.RemoteTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Scraper URL tests #58 for SpringerScraper
 *
 * @author tst
 */
@Category(RemoteTest.class)
public class SpringerScraperTest {
	String resultDirectory = "springer/springer/";

	/**
	 * starts URL test with id url_58
	 */
	@Test
	public void url1TestRun(){
		final String url = "https://www.springer.com/computer/database+management+&amp;+information+retrieval/book/978-0-387-95433-2";
		final String resultFile = resultDirectory + "SpringerScraperUnitURLTest1.bib";
		assertScraperResult(url, SpringerScraper.class, resultFile);
	}

	/**
	 * starts URL test with id url_239
	 */
	@Test
	public void url2TestRun() {
		final String url = "https://link.springer.com/content/pdf/10.1007%2F978-3-319-29671-5_3.pdf";
		final String resultFile = resultDirectory + "SpringerScraperUnitURLTest2.bib";
		assertScraperResult(url, SpringerScraper.class, resultFile);
	}

	/**
	 * starts URL test with id url_239
	 */
	@Test
	public void url3TestRun() {
		final String url = "https://link.springer.com/book/10.1007/978-0-387-85820-3";
		final String resultFile = resultDirectory + "SpringerScraperUnitURLTest3.bib";
		assertScraperResult(url, SpringerScraper.class, resultFile);
	}

	@Test
	public void url4TestRun() {
		final String url = "https://link.springer.com/book/10.1007%2F978-3-658-22345-8#about";
		final String resultFile = resultDirectory + "SpringerScraperUnitURLTest4.bib";
		assertScraperResult(url, SpringerScraper.class, resultFile);
	}

	@Test
	public void url5TestRun() {
		final String url = "https://www.springer.com/de/book/9783531199825#aboutBook";
		final String resultFile = resultDirectory + "SpringerScraperUnitURLTest5.bib";
		assertScraperResult(url, SpringerScraper.class, resultFile);
	}

}
