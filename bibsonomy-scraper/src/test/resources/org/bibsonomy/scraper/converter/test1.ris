TY  - CONF
AU  - Rendle, Steffen
AU  - Marinho, Leandro Balby
AU  - Nanopoulos, Alexandros
AU  - Schmidt-Thieme, Lars
A2  - 
T1  - Learning optimal ranking with tensor factorization for tag recommendation
T2  - KDD '09: Proceedings of the 15th ACM SIGKDD international conference on Knowledge discovery and data mining
PB  - ACM
C1  - New York, NY, USA
PY  - 2009/
CY  - 
IS  - 
SP  - 727
EP  - 736
UR  - http://portal.acm.org/citation.cfm?doid=1557019.1557100
DO  - 10.1145/1557019.1557100
KW  - factorization folkrank folksonomy learning optimal ranking tensor
L1  - 
SN  - 978-1-60558-495-9
N1  - Learning optimal ranking with tensor factorization for tag recommendation
N1  - 
AB  - Tag recommendation is the task of predicting a personalized list of tags for a user given an item. This is important for many websites with tagging capabilities like last.fm or delicious. In this paper, we propose a method for tag recommendation based on tensor factorization (TF). In contrast to other TF methods like higher order singular value decomposition (HOSVD), our method RTF ('ranking with tensor factorization') directly optimizes the factorization model for the best personalized ranking. RTF handles missing values and learns from pairwise ranking constraints. Our optimization criterion for TF is motivated by a detailed analysis of the problem and of interpretation schemes for the observed data in tagging systems. In all, RTF directly optimizes for the actual problem using a correct interpretation of the data. We provide a gradient descent algorithm to solve our optimization problem. We also provide an improved learning and prediction method with runtime complexity analysis for RTF. The prediction runtime of RTF is independent of the number of observations and only depends on the factorization dimensions. Besides the theoretical analysis, we empirically show that our method outperforms other state-of-the-art tag recommendation methods like FolkRank, PageRank and HOSVD both in quality and prediction runtime.
ER  -