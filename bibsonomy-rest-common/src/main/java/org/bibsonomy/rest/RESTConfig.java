/**
 * BibSonomy-Rest-Common - Common things for the REST-client and server.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bibsonomy.model.enums.GoldStandardRelation;

import static org.bibsonomy.util.ValidationUtils.present;

/**
 * DO NOT CHANGE any constant values after a release
 *
 * @author dzo
 */
public final class RESTConfig {

	private RESTConfig() {
		// noop
	}

	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

	public static final String DEFAULT_DATETIME_FORMAT = "yyyy-MM-DD HH:mm:ss";

	public static final String POSTS_URL = "posts";

	public static final String POSTS_ADDED_SUB_PATH = "added";

	public static final String RELATIONS_SUB_PATH = "relations";

	public static final String POSTS_PERSON_SUB_PATH = "person";

	public static final String POSTS_ADDED_URL = POSTS_URL + "/" + POSTS_ADDED_SUB_PATH;

	public static final String POSTS_POPULAR_SUB_PATH = "popular";

	public static final String PERIOD_INDEX = "periodIndex";

	public static final String COMMUNITY_SUB_PATH = "community";

	public static final String API_USER_AGENT = "BibSonomyWebServiceClient";

	public static final String SYNC_URL = "sync";

	public static final String CONCEPTS_URL = "concepts";

	public static final String TAGS_URL = "tags";

	public static final String RELATION_PARAM = "relation";

	public static final String RELATION_REFERENCE = GoldStandardRelation.REFERENCE.toString().toLowerCase();

	public static final String RELATION_PARTOF = GoldStandardRelation.PART_OF.toString().toLowerCase();

	public static final String USERS_URL = "users";

	public static final String PERSONS_URL = "persons";

	public static final String PROJECTS_URL = "projects";

	public static final String CRIS_LINKS_URL = "cris_links";

	public static final String PERSONS_MERGE_URL = "merge";

	public static final String DOCUMENTS_SUB_PATH = "documents";

	public static final String FRIENDS_SUB_PATH = "friends";

	public static final String FOLLOWERS_SUB_PATH = "followers";

	public static final String GROUPS_URL = "groups";

	public static final String RESOURCE_TYPE_PARAM = "resourcetype";

	public static final String RESOURCE_PARAM = "resource";

	public static final String TAGS_PARAM = "tags";

	public static final String FILTER_PARAM = "filter";

	public static final String SORT_KEY_PARAM = "sortkey";

	public static final String SORT_ORDER_PARAM = "sortorder";

	public static final String CONCEPT_STATUS_PARAM = "status";

	public static final String ORGANIZATION_PARAM = "organization";

	public static final String SEARCH_PARAM = "search";

	public static final String SEARCH_TYPE_PARAM = "searchtype";

	public static final String SUB_TAG_PARAM = "subtag";

	public static final String INTERHASH_PARAM = "interhash";

	public static final String INTRAHASH_PARAM = "intrahash";

	public static final String RELATION_TYPE_PARAM = "relationType";

	public static final String RELATION_INDEX_PARAM = "relationIndex";

	public static final String REGEX_PARAM = FILTER_PARAM;

	public static final String START_PARAM = "start";

	public static final String END_PARAM = "end";

	public static final String BEFORE_CHANGE_DATE_PARAM = "beforeChangeDate";

	public static final String AFTER_CHANGE_DATE_PARAM = "afterChangeDate";

	public static final String OPERATION_PARAM = "operation";

	public static final String SYNC_STRATEGY_PARAM = "strategy";

	public static final String SYNC_DIRECTION_PARAM = "direction";

	public static final String SYNC_DATE_PARAM = "date";

	public static final String SYNC_NEW_DATE_PARAM = "newDate";

	public static final String SYNC_STATUS = "status";

	public static final String CLIPBOARD_SUBSTRING = "clipboard";

	public static final String CLIPBOARD_CLEAR = "clear";

	/** the query param for a username */
	public static final String USER_PARAM = "user";

	/** the query param for a person id */
	public static final String PERSON_ID_PARAM = "personId";

	/** the query param for the source id of a person merge */
	public static final String SOURCE_ID_PARAM = "sourceId";

	/** the query param for the target id of a person merge */
	public static final String TARGET_ID_PARAM = "targetId";

	/** the query param name for an additional key for persons */
	public static final String PERSON_ADDITIONAL_KEY_PARAM = "additionalKey";

	/** the separator used to separate key and value */
	public static final String PERSON_ADDITIONAL_KEY_PARAM_SEPARATOR = "=";

	public static final String WITH_PERSONS_PARAM = "withPersons";

	public static final String WITH_POSTS_PARAM = "withPosts";

	public static final String WITH_PERSONS_OF_POSTS_PARAM = "withPersonsOfPosts";

	public static final String ONLY_THESES_PARAM = "onlyTheses";

	public static final String GROUP_BY_INTERHASH_PARAM = "groupByInterhash";


	/**
	 * Request Attribute ?relation="incoming/outgoing"
	 */
	public static final String ATTRIBUTE_KEY_RELATION = "relation";

	/** value for "incoming" */
	public static final String INCOMING_ATTRIBUTE_VALUE_RELATION = "incoming";

	/** value for "outgoing" */
	public static final String OUTGOING_ATTRIBUTE_VALUE_RELATION = "outgoing";

	/** default value */
	public static final String DEFAULT_ATTRIBUTE_VALUE_RELATION = INCOMING_ATTRIBUTE_VALUE_RELATION;

	/** place holder for the login user - used e.g. for OAuth requests */
	public static final String USER_ME = "@me";

	public static final String REMOTE_USER_ID = "remoteUserId";

	public static final String IDENTITY_PROVIDER = "identityProvider";

	public static final String IDENTITY_PROVIDER_TYPE = "identityProviderType";

	/**
	 * serialize a date with default date format for param
	 * @param date		date
	 * @return			serialized date
	 */
	public static String serializeDate(final Date date) {
		if (present(date)) {
			final DateFormat fmt = new SimpleDateFormat(RESTConfig.DEFAULT_DATE_FORMAT);
			return fmt.format(date);
		}
		return null;
	}

	/**
	 * serialize a date with default datetime format for param
	 * @param date		date
	 * @return			serialized datetime
	 */
	public static String serializeDateTime(final Date date) {
		if (present(date)) {
			final DateFormat fmt = new SimpleDateFormat(RESTConfig.DEFAULT_DATETIME_FORMAT);
			return fmt.format(date);
		}
		return null;
	}

}
