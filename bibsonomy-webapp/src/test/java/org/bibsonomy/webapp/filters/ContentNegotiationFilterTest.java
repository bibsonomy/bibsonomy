/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.filters;

import static org.bibsonomy.util.ValidationUtils.present;
import static org.junit.Assert.assertArrayEquals;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;

import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.bibsonomy.util.StringUtils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author rja
 */
public class ContentNegotiationFilterTest {

	private static final int PORT = 31416;


	private static Server tester;
	private static String baseUrl;
	private static HttpClient client;


	/**
	 * start jetty container
	 *
	 * @throws Exception
	 */
	@BeforeClass
	public static void initServletContainer () throws Exception {
		tester = new Server(PORT);
		final ServletContextHandler handler = new ServletContextHandler();
		handler.setContextPath("/");
		handler.addFilter(ContentNegotiationFilter.class, "/*", EnumSet.allOf(DispatcherType.class));
		handler.addServlet(TestServlet.class, "/*");

		tester.setHandler(handler);
		baseUrl = "http://localhost:" + PORT;
		tester.start();

		final HttpClientBuilder builder = HttpClientBuilder.create().disableRedirectHandling();
		client = builder.build();
	}

	/**
	 * stop jetty container
	 * @throws Exception
	 */
	@AfterClass
	public static void cleanupServletContainer () throws Exception {
		tester.stop();
	}

	/**
	 * test the doFilter() method
	 *
	 * @throws Exception
	 */
	@Test
	public void testDoFilter () throws Exception {
		/*
		 * simple cases: here we want redirects
		 */
		assertArrayEquals(new String[]{"302", "/burst/user/jaeschke"}, sendHttpGet(baseUrl + "/user/jaeschke", null, "application/rdf+xml"));
		assertArrayEquals(new String[]{"302", "/json/user/jaeschke"}, sendHttpGet(baseUrl + "/user/jaeschke", null, "application/json"));
		assertArrayEquals(new String[]{"302", "/csv/user/jaeschke"}, sendHttpGet(baseUrl + "/user/jaeschke", null, "text/csv"));
		assertArrayEquals(new String[]{"302", "/bib/user/jaeschke"}, sendHttpGet(baseUrl + "/user/jaeschke", null, "text/x-bibtex"));
		/*
		 * more complicated: priorities
		 */
		assertArrayEquals(new String[]{"302", "/burst/user/jaeschke"}, sendHttpGet(baseUrl + "/user/jaeschke", null, "application/rdf+xml,application/xhtml+xml,text/html;q=0.9,*/*;q=0.8;"));

		/*
		 * per default, API calls and static resources are ignored
		 */
		assertArrayEquals(new String[]{"200"}, sendHttpGet(baseUrl + "/resources/css/style.css", null, "application/rdf+xml"));
		assertArrayEquals(new String[]{"200"}, sendHttpGet(baseUrl + "/api/users", null, "application/rdf+xml"));

		/*
		 * no redirect when format is explicitly specified
		 */
		assertArrayEquals(new String[]{"200"}, sendHttpGet(baseUrl + "/burst/user/jaeschke", null, "application/rdf+xml"));
		assertArrayEquals(new String[]{"200"}, sendHttpGet(baseUrl + "/publ/user/jaeschke", null, "application/rdf+xml"));
		assertArrayEquals(new String[]{"200"}, sendHttpGet(baseUrl + "/user/jaeschke", Collections.singletonMap("format", "bib"), "application/rdf+xml"));

		/*
		 * no specific format requested - get HTML page
		 * (some typical headers sent by web browsers)
		 */
		assertArrayEquals(new String[]{"200"}, sendHttpGet(baseUrl + "/user/jaeschke", null, "text/xhtml"));
		assertArrayEquals(new String[]{"200"}, sendHttpGet(baseUrl + "/user/jaeschke", null, "text/css,*/*;q=0.1"));
		assertArrayEquals(new String[]{"200"}, sendHttpGet(baseUrl + "/user/jaeschke", null, "*/*"));
		assertArrayEquals(new String[]{"200"}, sendHttpGet(baseUrl + "/user/jaeschke", null, "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"));

		/*
		 * specific format requested but not with highest priority
		 */
		assertArrayEquals(new String[]{"200"}, sendHttpGet(baseUrl + "/user/jaeschke", null, "text/html,application/xhtml+xml,application/rdf+xml;q=0.9,*/*;q=0.8;"));
	}

	/**
	 * Sends GET requests to the specified URL with the given query string and
	 * accept header value.
	 *
	 * @param url
	 * @param query
	 * @param accept
	 * @return An array containing the HTTP response code as first value. If the
	 * code is a redirect (302), the second value contains the location the
	 * redirect points to.
	 *
	 * @throws Exception
	 */
	private String[] sendHttpGet (final String url, final Map<String, String> query, final String accept) throws Exception {
		try {
			final URIBuilder builder = new URIBuilder(url);
			if (present(query)) {
				for (Map.Entry<String, String> entry : query.entrySet()) {
					builder.setParameter(entry.getKey(), entry.getValue());
				}
			}
			final HttpGet get = new HttpGet(builder.build());
			get.setHeader("Accept", accept);

			final HttpResponse response = client.execute(get);

			final int statusCode = response.getStatusLine().getStatusCode();
			final String[] result;
			switch (statusCode) {
			case 302:
				result = new String[]{statusCode + "", stripHost(response.getFirstHeader("Location").getValue())};
				break;
			default:
				result = new String[]{statusCode + ""};
			}
			get.releaseConnection();
			return result;
		} catch (final Exception e) {
			throw new Exception("request failed", e);
		}
	}

	private String stripHost (final String url) {
		return url.replace(baseUrl, "");
	}

	/**
	 * Only for testing purposes - does nothing useful.
	 *
	 * @author rja
	 *
	 */
	public static class TestServlet extends HttpServlet {
		/**
		 *
		 */
		private static final long serialVersionUID = 8692283813700271210L;

		@Override
		protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
			new BufferedWriter(new OutputStreamWriter(resp.getOutputStream(), StringUtils.CHARSET_UTF_8)).write("Hello World!");
		}
	}

}
