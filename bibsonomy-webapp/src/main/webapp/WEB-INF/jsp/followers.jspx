<?xml version="1.0" ?>
<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:tags="urn:jsptagdir:/WEB-INF/tags/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:user="urn:jsptagdir:/WEB-INF/tags/resources/user"
	xmlns:users="urn:jsptagdir:/WEB-INF/tags/users"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
	xmlns:sidebar="urn:jsptagdir:/WEB-INF/tags/layout/sidebar"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld">
	
	<jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true" />
	
	<c:set var="requPathAndQuery" value="/${requPath}${requQueryString}"/>
	<layout:resourceLayout pageTitle="${command.pageTitle}"	command="${command}" requPath="${requPath}"	activeTab="${command.context.loginUser.name != null ? 'my' : ''}">

		<jsp:attribute name="heading">
			<parts:search pathMessageKey="navi.followees"/>
		</jsp:attribute>
		
		<jsp:attribute name="additionalSorting">
			<!-- ranking sorting -->
			<c:choose>
				<c:when test="${command.sortPage eq 'rank' and command.sortPageOrder eq 'desc'}">
					<li class="sort-selection sort-selected"><a href="${mtl:setParam(mtl:setParam(requPathAndQuery, 'sortPage', 'rank'), 'sortPageOrder', 'asc')}">
						<fontawesome:icon icon="sort-amount-down" spaceAfter="5"/><fmt:message key="ranking.method"/><fontawesome:icon icon="arrow-down" spaceBefore="5"/>
					</a></li>
				</c:when>
				<c:when test="${command.sortPage eq 'rank' and command.sortPageOrder eq 'asc'}">
					<li class="sort-selection sort-selected"><a href="${mtl:setParam(mtl:setParam(requPathAndQuery, 'sortPage', 'rank'), 'sortPageOrder', 'desc')}">
						<fontawesome:icon icon="sort-amount-up" spaceAfter="5"/><fmt:message key="ranking.method"/><fontawesome:icon icon="arrow-up" spaceBefore="5"/>
					</a></li>
				</c:when>
				<c:otherwise>
					<li class="sort-selection"><a href="${mtl:setParam(mtl:setParam(requPathAndQuery, 'sortPage', 'rank'), 'sortPageOrder', 'desc')}">
						<fontawesome:icon icon="sort-amount-down" spaceAfter="5" /><fmt:message key="ranking.method"/>
					</a></li>
				</c:otherwise>
			</c:choose>
		</jsp:attribute>

		<jsp:attribute name="sidebar">
			<!--  ranking settings -->
			<c:if test="${command.sortKey eq 'RANK'}">
				<fmt:message key="navi.go" var="go" />
				<sidebar:sidebarItem textKey="navi.settings">
					<jsp:attribute name="content">
						<form:form class="form" method="GET" action="/followers">
							<fmt:message key="ranking.method"/>:<c:out value=" "/>
							<form:select path="ranking.method">
							<form:option value="tfidf"><fmt:message key="ranking.tfidf"/></form:option>
								<form:option value="tag_overlap"><fmt:message key="ranking.tag_overlap"/></form:option>
							</form:select>
							<br/>
							<fmt:message key="ranking.normalize"/>:<c:out value=" "/>
							<form:select path="ranking.normalize">
								<form:option value="true"><fmt:message key="yes"/></form:option>
								<form:option value="false"><fmt:message key="no"/></form:option>
							</form:select>
							<input type="hidden" name="requestedUser" value="${command.requestedUser}"/>
							<br/>
							<input class="btn btn-default btn-sm" type="submit" name="submit" value="${go}"/>
						</form:form>
					</jsp:attribute>
				</sidebar:sidebarItem>
			</c:if>
			
			<c:set var="urlGeneratorPersonalized" value="${urlGeneratorFactory.createURLGeneratorForPrefix(null, 'personalized')}" />
			<script type="text/javascript" src="${resdir}/javascript/userRelation.js"><!-- keep me --></script>
			<sidebar:sidebarItem textKey="follow.offollower">
				<jsp:attribute name="content">
					<ul id="followedUsers" class="sidebar_collapse_content">
						<c:forEach var="user" items="${command.followersOfUser}">
							<li>
								<a href="${urlGeneratorPersonalized.getUserUrlByUserName(user.name)}">
									<c:out value='${user.name}' />
								</a>&amp;nbsp;
								<fmt:message var="deleteFriendTitle" key="follow.actions.delete.title">
									<fmt:param value="${fn:escapeXml(user.name)}" />
								</fmt:message>
								<a class="action btn btn-danger btn-xs" title="${deleteFriendTitle}" href="/ajax/handleUserRelation?requestedUserName=${user.name}&amp;action=removeFollower&amp;ckey=${ckey}&amp;forward=followers">
									<fmt:message key="follow.link.text.remove" />
								</a>
							</li>
						</c:forEach>
					</ul>
				</jsp:attribute>
			</sidebar:sidebarItem>
			
			<!-- display similar users -->
			<sidebar:sidebarItem textKey="follow.whotofollow">
				<jsp:attribute name="content">
					<h5><fmt:message key="users.similar.your" /></h5>
					<a href="${mtl:setParam(requPathAndQuery, 'userSimilarity', mtl:toggleUserSimilarity(command.userSimilarity)) }"><fmt:message key="more"/>...</a>
					<users:related cmd="${command.relatedUserCommand}" displayFollowLink="${true}" personalized="${true}" userSimilarity="${command.userSimilarity}"/>
					
					<h5><fmt:message key="follow.followerof" /></h5>
					<ul class="sidebar_collapse_content">
						<c:forEach var="user" items="${command.userIsFollowing}">
							<li>
								<a href="${urlGeneratorPersonalized.getUserUrlByUserName(user.name)}"><c:out value="${user.name}" /></a>
								<c:choose>
									<c:when test="${not mtl:contains(command.followersOfUser, user)}">
										<c:out value=" " />
										<a class="action btn btn-default btn-xs" title="${addFriendTitle}" href="/ajax/handleUserRelation?requestedUserName=${fn:escapeXml(user.name)}&amp;action=addFollower&amp;ckey=${ckey}&amp;forward=followers">
											<fmt:message key="follow.link.text.follow" />
										</a>
									</c:when>
									<c:otherwise>
										<c:out value=" " />
										<a class="action btn btn-danger btn-xs" title="${deleteFriendTitle}" href="/ajax/handleUserRelation?requestedUserName=${user.name}&amp;action=removeFollower&amp;ckey=${ckey}&amp;forward=followers">
											<fmt:message key="follow.link.text.remove" />
										</a>
									</c:otherwise>
								</c:choose>
								
							</li>
						</c:forEach>
					</ul>
				</jsp:attribute>
			</sidebar:sidebarItem>
		</jsp:attribute>
		
		<!-- content heading -->
		<jsp:attribute name="infobox">
			<parts:infobox>
				<jsp:attribute name="infoTextBody">
					<fmt:message key="followersPage.about">
						<fmt:param>${properties['project.home']}help_${locale.language}/Followers</fmt:param>
					</fmt:message>
					<c:if test="${command.sortKey eq 'RANK'}">
						<c:out value=" "/>
						<c:set var="ofUsers">
							<c:choose>
								<c:when test="${empty command.requestedUser or command.requestedUser eq ''}">
									<fmt:message key="followersPage.entries.ofAllFollowedUsers"/>
								</c:when>
							<c:otherwise>
								<fmt:message key="followersPage.entries.ofUser">
									<fmt:param><a href="${relativeUrlGenerator.getUserUrlByUserName(fn:escapeXml(command.requestedUser))}"><c:out value='${command.requestedUser}'/></a></fmt:param>
								</fmt:message>
							</c:otherwise>
							</c:choose>
						</c:set>
						<c:choose>
							<c:when test="${empty command.ranking.period or command.ranking.period == 0}">
								<fmt:message key="followersPage.all.mostRecent.contentHeading">
									<fmt:param>${command.ranking.periodEnd}</fmt:param>
									<fmt:param>${ofUsers}</fmt:param>
								</fmt:message>
							</c:when>
							<c:otherwise>
								<fmt:message key="followersPage.all.recent.contentHeading">
									<fmt:param>${command.ranking.periodStart}</fmt:param>
									<fmt:param>${command.ranking.periodEnd}</fmt:param>
									<fmt:param>${ofUsers}</fmt:param>
								</fmt:message>
							</c:otherwise>
						</c:choose>
						<c:out value=" "/>
						<fmt:message key="Show"/>
						<c:out value=" "/>
						<c:set var="requPathAndQuery" value="${requPath}${requQueryString}"/>
						<c:if test="${not empty command.ranking.period and command.ranking.period > 0}">
							<a href="${mtl:setParam(requPathAndQuery, 'ranking.period', command.ranking.prevPeriod)}" ><fmt:message key="younger"/></a>
							${mtl:ch('nbsp')}<c:out value="|"/>${mtl:ch('nbsp')}
						</c:if>
						<a href="${mtl:setParam(requPathAndQuery, 'ranking.period', command.ranking.nextPeriod)}" ><fmt:message key="older"/></a>
						<c:out value=" "/>
						<fmt:message key="posts"/>.
					</c:if>
				</jsp:attribute>
			</parts:infobox>
		</jsp:attribute>
	</layout:resourceLayout>
</jsp:root>