<?xml version="1.0" ?>
<jsp:root version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:bs="urn:jsptagdir:/WEB-INF/tags/bs"
	xmlns:post="urn:jsptagdir:/WEB-INF/tags/post"
	xmlns:batch="urn:jsptagdir:/WEB-INF/tags/actions/edit/batch"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:bib="urn:jsptagdir:/WEB-INF/tags/resources/bibtex">
	
	<jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true" />	
	<fmt:message key="post_bibtex.heading" var="msgPostPublication"/>
	
	<c:set var="isadmin" value="${command.context.loginUser.role eq 'ADMIN'}" />
	<layout:layout 
		loginUser="${command.context.loginUser}" 
		pageTitle="${pageTitle}"
		noSidebar="${true}"
		requPath="${requPath}">
		
		<jsp:attribute name="headerExt">
			<script type="text/javascript" src="${resdir}/javascript/configuration.js"><!-- keep me --></script>
			<script type="text/javascript" src="${resdir}/javascript/postPublication.js"><!-- keep me --></script>
			<script type="text/javascript" src="${resdir}/javascript/customEntrytypes.js"><!-- keep me --></script>
		</jsp:attribute>
		
		<jsp:attribute name="content">
			<div id="post-publication" class="row">
				<div class="col-sm-12">
					<h1 class="content-heading"><fmt:message key="publication.import.title"/></h1>
					
					<c:choose>
						<c:when test="${not empty command.bibtex and not empty command.bibtex.list}">
							<c:set var="hasErrors" value="false" />
							<c:set var="hasValidData" value="false" />
							<c:forEach var="errorInfo" items="${command.groupedErrorList}">
								<c:if test="${errorInfo.errorType eq 'NO_ERROR'}">
									<c:set var="hasValidData" value="true" />
								</c:if>
								<c:if test="${errorInfo.errorType eq 'FIELD_ERROR' or errorInfo.errorType eq 'POST_ERROR'}">
									<c:set var="hasErrors" value="true" />
								</c:if>
							</c:forEach>
							<div id="parsedPostView">
								<p class="smalltext" style="margin-top:1em; margin-bottom:1em">
									<c:choose>
										<c:when test="${hasErrors and hasValidData}">
											<fmt:message key="import.error.validData_and_parse_or_incomplete_bibtex.description">
												<fmt:param value="${properties['project.name']}" />
											</fmt:message>
										</c:when>
										<c:when test="${hasErrors}">
											<fmt:message key="import.error.parse_or_incomplete_bibtex.description">
												<fmt:param value="${properties['project.name']}" />
											</fmt:message>
										</c:when>
									</c:choose>
								</p>
								<fieldset>
									<batch:importContent	resourcetype="bibtex"
															listView="${command.bibtex}"
															otherPostsUrlPrefix=""
															listViewStartParamName="bibtex.start"
															postsErrorMessages="${command.postsErrorList}"
															directEdit="${false}"
															overwrite="${command.overwrite}"
															groupedErrorList="${command.groupedErrorList}"
															updateExistingPost="${command.updateExistingPost}"
															hasErrors="${hasErrors}"
															hasValidData="${hasValidData}">
										<jsp:attribute name="shortPostDescription">
											<bib:shortDescription post="${post}"/>
										</jsp:attribute>
									</batch:importContent>
								</fieldset>
							</div>
						</c:when>
						
						<c:otherwise>
							<post:postPublicationUpload />
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</jsp:attribute>
		
		<!--+
			| infobox
			+-->
		<jsp:attribute name="infobox">
			<parts:infobox>
				<jsp:attribute name="infoTextBody">
					<bs:didYouKnow command="${command}" />
				</jsp:attribute>
			</parts:infobox>
		</jsp:attribute>
	</layout:layout>
</jsp:root>