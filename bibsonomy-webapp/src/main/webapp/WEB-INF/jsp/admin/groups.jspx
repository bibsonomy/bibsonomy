<?xml version="1.0" ?>
<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:tags="urn:jsptagdir:/WEB-INF/tags/users"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:menu="urn:jsptagdir:/WEB-INF/tags/menu"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:bs="urn:jsptagdir:/WEB-INF/tags/bs"
	xmlns:nav="urn:jsptagdir:/WEB-INF/tags/nav"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld">

	<jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true" />

	<fmt:message key="navi.admin" var="adminPageTitle" />
	<fmt:message key="navi.admin.groups" var="groupsTitle" />

	<layout:layout 
		pageTitle="${groupsTitle} | ${adminPageTitle}"
		loginUser="${command.context.loginUser}" 
		requPath="${requPath}">

		<jsp:attribute name="breadcrumbs">
			<nav:breadcrumbs>
				<jsp:attribute name="crumbs">
					<nav:admin />
					<nav:crumb name="${groupsTitle}" active="true" />
				</jsp:attribute>
			</nav:breadcrumbs>
		</jsp:attribute>
		
		<jsp:attribute name="heading">
			<parts:search pathMessageKey="navi.search" formAction="/search" formInputName="search"/>
		</jsp:attribute>

		<jsp:attribute name="headerExt">
			<link rel="stylesheet" href="${resdir}/jquery/plugins/ui/jquery-ui.css" type="text/css" media="all" />
			<link rel="stylesheet" href="${resdir}/jquery/plugins/ui/ui.theme.css" type="text/css" media="all" />

			<script type="text/javascript" src="${resdir}/jquery/plugins/form/jquery.form.js"><!-- keep me --></script> 
			<!--  scripts for ajax autocompletion -->
			<script src="${resdir}/jquery/plugins/bgiframe/jquery.bgiframe.js" type="text/javascript"><!--  --></script>
			<script src="${resdir}/jquery/plugins/ui/jquery-ui.js" type="text/javascript"><!--  --></script>
			<script src="${resdir}/jquery/plugins/ui/jquery-ui-i18n.js" type="text/javascript"><!--  --></script>
			
			<!-- declineGroup helper -->
			<script src="${resdir}/javascript/admin/groups.js" type="text/javascript"><!--  --></script>
			
			<!--  restoreGroup helper -->
			<script type="text/javascript" src="${resdir}/javascript/requestGroupAutocomplete.js"><!-- keep me --></script> 
		</jsp:attribute>

		<jsp:attribute name="content">

			<!-- BS3 this. -->
			<c:if test="${not empty command.adminResponse}">
			<div>
				<bs:feedback size="defaultSize" style="success">
					<jsp:attribute name="feedbackBody">
						<c:out value="${command.adminResponse}" />
					</jsp:attribute>
				</bs:feedback>
			</div>
			</c:if>
			
			<bsform:fieldset legendKey="settings.group.reactivate">
				<jsp:attribute name="content">
					<fmt:message key="settings.group.reactivate.description" />
					<form:form name="groupInfo" action="${relativeUrlGenerator.getAdminUrlByName('groups')}" method="POST" cssClass="form-horizontal">
						<input type="hidden" value="RESTORE_GROUP" name="action" />
						<bsform:select path="group.name" itemsList="${command.allDeletedGroupNames}" labelColSpan="3" inputColSpan="9" id="fetchGroupInfo" />
						
						<bsform:input path="group.groupRequest.userName" labelColSpan="3" inputColSpan="9" id="designatedAdmin" helpKey="settings.group.reactivate.help" autocomplete="true" labelKey="group.groupRequest.adminName" />
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<fmt:message key="settings.group.reactivate" var="reactivateButtonText" />
								<bsform:button type="submit" style="primary" size="defaultSize" value="${reactivateButtonText}" />
							</div>
						</div>
					</form:form>
				</jsp:attribute>
			</bsform:fieldset>				
			
			<bsform:fieldset legendKey="settings.group.delete">
				<jsp:attribute name="content">
					<fmt:message key="settings.group.delete.description" />
					<form:form name="groupInfo" action="${relativeUrlGenerator.getAdminUrlByName('groups')}" method="POST" cssClass="form-horizontal">
						<input type="hidden" value="DELETE_GROUP" name="action" />
						<bsform:select path="group.name" itemsList="${command.allGroupNames}" labelColSpan="3" inputColSpan="9" id="fetchGroupInfo" />
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<fmt:message key="settings.group.delete" var="buttonText" />
								<bsform:button type="submit" style="primary" size="defaultSize" value="${buttonText}" />
							</div>
						</div>
					</form:form>
				</jsp:attribute>
			</bsform:fieldset>

			<bsform:fieldset legendKey="settings.group.permissionHeading">
				<jsp:attribute name="content">

					<form:form name="groupWithPermissions" action="${relativeUrlGenerator.getAdminUrlByName('groups')}" method="POST" cssClass="form-horizontal" >
						<input type="hidden" value="UPDATE_PERMISSIONS" name="action" />
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<p class="help-block"><fmt:message key="settings.group.permissionIntro"/></p>
							</div>
						</div>
						
						<bsform:input labelColSpan="3" inputColSpan="9" id="fetchGroupForPermissions" path="group.name" helpKey="settings.group.permission.help" autocomplete="true" />
						
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<p class="form-control-static"><a title="fetch group permissions" style="cursor: pointer" onclick="javascript:fetchGroupPermissions('#fetchGroupForPermissions');"><fmt:message key="settings.group.permission.fetch"/></a></p>
							</div>
							<fmt:message key="group.updatePermission.update" var="msg"/>
						</div>
						
						<bsform:checkbox inputColSpan="9" id="COMMUNITY_POST_INSPECTION" path="communityPostInspectionPermission" labelKey="settings.group.permission.communityPostInspectionPermission" disabled="true" classAtt="permissionCheckbox"/>

						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<bsform:button type="submit" style="primary" size="defaultSize" value="${msg}" />
							</div>
						</div>

					</form:form>
				</jsp:attribute>
			</bsform:fieldset>
				
			<c:if test="${not empty command.pendingGroups}">
				
				<fmt:message var="declineMsg" key="admin.actions.group.decline"/>
				<fmt:message var="cancelMsg" key="settings.cancel"/>
				
				<bsform:fieldset legendKey="admin.group.pending">
					<jsp:attribute name="content">
						<table class="table table-hover">
							<tr>
								<th>group name</th>
								<th>requesting user</th>
								<th>request reason</th>
								<th>request date</th>
								<th>&amp;nbsp;</th>
							</tr>
							<c:forEach var="entry" items="${command.pendingGroups}">
								<tr>
									<td>
										<c:out value="${entry.name}"/>
									</td>
									<!-- show request user name -->
									<td>
										<a href="${relativeUrlGenerator.getUserUrlByUserName(entry.groupRequest.userName)}">
											<c:out value="${entry.groupRequest.userName}"/>
										</a>
									</td>
									<!-- show request reason -->
									<td>
										<c:out value="${entry.groupRequest.reason}"/>
									</td>
									<!-- show request submission date -->
									<td>
										<c:out value="${entry.groupRequest.submissionDate}"/>
									</td>
									<td>
										<span class="groupsButtons">
											<a href="/admin/groups?action=ACCEPT_GROUP&amp;group.name=${fn:escapeXml(entry.name)}"><fmt:message key="admin.actions.group.accept"/></a>
											${mtl:ch('nbsp')}
											<a href="javascript:void(0)" class="declineGroupExpand" onclick="declineGroup(this);"><fmt:message key="admin.actions.group.decline"/></a>	
										</span>
									</td>
								</tr>
					
								<tr class="groupDeclineRow">
									<td colspan="5">
										
										<fmt:message var="decLabelText" key="admin.actions.group.decline.reason">
											<fmt:param value="${fn:escapeXml(entry.name)}" />
										</fmt:message>
									
										<form:form name="declineGroupRequest" action="/admin/groups?action=DECLINE_GROUP&amp;group.name=${fn:escapeXml(entry.name)}" method="POST" cssClass="form-horizontal" >
											<bsform:textarea noHelp="true" labelText="${decLabelText}" path="declineMessage" labelColSpan="3" inputColSpan="9" />
											
											<div class="form-group">
												<div class="col-sm-offset-3 col-sm-9">
													<bsform:button type="submit" style="primary" size="defaultSize" value="${declineMsg}" />
													${mtl:ch('nbsp')}
													<bsform:button type="reset" style="primary" size="defaultSize" className="btn-danger declineGroupCancel" value="${cancelMsg}" />
												</div>
											</div>
										</form:form>
									</td>
								</tr>
								
							</c:forEach>
						</table>
					</jsp:attribute>
				</bsform:fieldset>
			</c:if>

		</jsp:attribute>
	</layout:layout>
</jsp:root>