<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<web-app xmlns="http://java.sun.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
	version="3.0">

	<!-- This flag is to mark the application as distributable, which implies 
		among others that all objects which shall be stored in a session need to 
		be serializable.-->
	<distributable />

	<!-- Initialization parameters -->
	<display-name>bibsonomy</display-name>
	<session-config>
		<session-timeout>60</session-timeout>
	</session-config>
	
	<context-param>
		<param-name>config.location</param-name>
		<param-value>${user.home}/bibsonomy.properties</param-value>
	</context-param>

	<!-- error pages -->
	<error-page>
		<error-code>404</error-code>
		<location>/errors/404</location>
	</error-page>
	<error-page>
		<error-code>405</error-code>
		<location>/errors/405</location>
	</error-page>
	<error-page>
		<error-code>500</error-code>
		<location>/errors/500</location>
	</error-page>
	<error-page>
		<error-code>503</error-code>
		<location>/errors/503</location>
	</error-page>

	<!-- filters -->
	<filter>
		<filter-name>encodingFilter</filter-name>
		<filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
		<init-param>
			<param-name>encoding</param-name>
			<param-value>UTF-8</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>encodingFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>

	<filter>
		<description>Removes the context path from all request URLs.</description>
		<filter-name>contextPathFilter</filter-name>
		<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
		<init-param>
			<param-name>contextAttribute</param-name>
			<param-value>org.springframework.web.servlet.FrameworkServlet.CONTEXT.bibsonomy</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>contextPathFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>

	<filter>
		<description>Evaluates the Accept-Header for Linked-Open-Data Content Negotiation. Sends Redirects.</description>
		<filter-name>ContentNegotiationFilter</filter-name>
		<filter-class>org.bibsonomy.webapp.filters.ContentNegotiationFilter</filter-class>
		<init-param>
			<description>The pattern defines, which URLs should not be touched by the filter.</description>
			<param-name>excludePattern</param-name>
			<param-value>^/(api|resources|resources_puma|ajax|layoutinfo)/.*</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>ContentNegotiationFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>

	<filter>
		<filter-name>springSecurityFilterChain</filter-name>
		<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
		<init-param>
			<param-name>contextAttribute</param-name>
			<param-value>org.springframework.web.servlet.FrameworkServlet.CONTEXT.bibsonomy</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>springSecurityFilterChain</filter-name>
		<url-pattern>/*</url-pattern>
		<dispatcher>REQUEST</dispatcher>
		<dispatcher>ERROR</dispatcher>
	</filter-mapping>

	<filter>
		<description>Converts posted method parameters into HTTP methods (parameter name: _method)</description>
		<filter-name>hiddenHttpMethodFilter</filter-name>
		<filter-class>org.springframework.web.filter.HiddenHttpMethodFilter</filter-class>
	</filter>
	<filter-mapping>
		<filter-name>hiddenHttpMethodFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>

	<filter>
		<filter-name>ActionValidationFilter</filter-name>
		<filter-class>filters.ActionValidationFilter</filter-class>
	</filter>
	<filter-mapping>
		<filter-name>ActionValidationFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>

	<filter>
		<filter-name>CacheFilter</filter-name>
		<filter-class>org.bibsonomy.webapp.filters.CacheFilter</filter-class>
		<init-param>
			<description>The pattern defines, which files can be cached.</description>
			<param-name>cachePattern</param-name>
			<param-value>^/(resources|resources_puma)/.*</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>CacheFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>

	<filter>
		<filter-name>UrlRewriteFilter</filter-name>
		<filter-class>org.tuckey.web.filters.urlrewrite.UrlRewriteFilter</filter-class>
		<init-param>
			<param-name>logLevel</param-name>
			<param-value>log4j</param-value>
		</init-param>
		<init-param>
			<param-name>confReloadCheckInterval</param-name>
			<param-value>-1</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>UrlRewriteFilter</filter-name>
		<url-pattern>/*</url-pattern>
		<dispatcher>REQUEST</dispatcher>
	</filter-mapping>

	<!-- spring main servlet -->
	<servlet>
		<servlet-name>bibsonomy</servlet-name>
		<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
		<load-on-startup>1</load-on-startup>
	</servlet>
	
	<!-- spring main servlet -->
	<servlet-mapping>
		<servlet-name>bibsonomy</servlet-name>
		<url-pattern>/</url-pattern>
	</servlet-mapping>
	
	<servlet-mapping>
		<servlet-name>default</servlet-name>
		<url-pattern>/favicon.ico</url-pattern>
	</servlet-mapping>
	<servlet-mapping>
		<servlet-name>default</servlet-name>
		<url-pattern>/robots.txt</url-pattern>
	</servlet-mapping>
	<servlet-mapping>
		<servlet-name>default</servlet-name>
		<url-pattern>/resources/*</url-pattern>
	</servlet-mapping>
	<servlet-mapping>
		<servlet-name>default</servlet-name>
		<url-pattern>/resources_puma/*</url-pattern>
	</servlet-mapping>
	<servlet-mapping>
		<servlet-name>default</servlet-name>
		<url-pattern>/brokenurl.html</url-pattern>
	</servlet-mapping>

	<jsp-config>
		<jsp-property-group>
			<description></description>
			<url-pattern>*.jspx</url-pattern>
			<is-xml>true</is-xml>
		</jsp-property-group>
	</jsp-config>
</web-app>