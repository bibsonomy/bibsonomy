<?xml version="1.0" ?>
<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
          xmlns:jsp="http://java.sun.com/JSP/Page"
          xmlns:c="http://java.sun.com/jsp/jstl/core"
          xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
          xmlns:fn="http://java.sun.com/jsp/jstl/functions"
          xmlns:bs="urn:jsptagdir:/WEB-INF/tags/bs"
          xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
          xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
          xmlns:fontawsome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
          xmlns:adminSearch="urn:jsptagdir:/WEB-INF/tags/admin/search">

    <jsp:directive.attribute type="java.lang.String" name="entity" required="true" description="Indices infromation for this entity" />
    <jsp:directive.attribute type="java.util.List" name="searchIndexInfos" required="true" description="List of search index information" />
    <jsp:directive.attribute type="java.lang.Boolean" name="showWarning" required="false" description="Flag to show warnings about the number of indices" />
    <jsp:directive.attribute type="java.lang.Boolean" name="showActions" required="false" description="Flag to show action buttons for indices" />

    <h3>
        <fmt:message key="admin.search.index.title">
            <fmt:param value="${entity}"/>
        </fmt:message>
    </h3>
    <c:set var="searchIndexInfos" value="${command.searchIndexInfo.get(entity)}"/>
    <c:set var="searchIndexInfosSize" value="${fn:length(searchIndexInfos)}"/>

    <div class="searchindex-panels">
        <c:forEach var="searchIndexInfo" items="${searchIndexInfos}">
            <adminSearch:indexInfo entity="${entity}" searchIndexInfo="${searchIndexInfo}" showActions="${showActions}"/>
        </c:forEach>
    </div>

    <c:if test="${showWarning}">
        <!-- Warning for none or one existing index -->
        <c:choose>
            <c:when test="${searchIndexInfosSize eq 0}">
                <bs:alert style="warning">
                    <jsp:attribute name="alertBody">
                        <fmt:message key="admin.search.index.none"/>
                    </jsp:attribute>
                </bs:alert>
            </c:when>

            <c:when test="${searchIndexInfosSize eq 1}">
                <bs:alert style="warning">
                    <jsp:attribute name="alertBody">
                        <fmt:message key="admin.search.index.one"/>
                    </jsp:attribute>
                </bs:alert>
            </c:when>
        </c:choose>
    </c:if>

    <c:if test="${showActions}">
        <form method="post" class="searchindex-actions-form">
            <input type="hidden" name="entity" value="${entity}"/>
            <input type="hidden" name="action" value="generate_index"/>
            <fmt:message var="generateMsg" key="admin.search.index.generate">
                <fmt:param value="${entity}"/>
            </fmt:message>
            <bsform:button size="defaultSize" style="defaultStyle" value="${generateMsg}" type="submit"/>
        </form>

        <form method="post" class="searchindex-actions-form">
            <input type="hidden" name="entity" value="${entity}" />
            <input type="hidden" name="action" value="regenerate_all_indices" />
            <fmt:message var="confirmRegenerateAll" key="admin.search.actions.regenerateAll.confirm"/>
            <button type="submit" class="btn btn-warning" onclick="return confirm('${confirmRegenerateAll}')">
                <fmt:message key="admin.search.actions.regenerateAll"/>
            </button>
        </form>
    </c:if>

    <hr/>
</jsp:root>
