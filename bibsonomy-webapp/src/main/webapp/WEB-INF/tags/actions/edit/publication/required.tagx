<jsp:root version="2.0"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:bibtex="urn:jsptagdir:/WEB-INF/tags/resources/bibtex"
	xmlns:publication="urn:jsptagdir:/WEB-INF/tags/actions/edit/publication">
    <!--

        Shows the required form fields entrytype, bibtexkey, title, author,
          editor, year for publications

     -->

	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true"/>
	<jsp:directive.attribute name="hideBibTeXKey" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="hideAuthorClaim" type="java.lang.Boolean" required="false"/>

	<jsp:directive.attribute name="personIndex" type="java.lang.Integer" required="false"/>
	<jsp:directive.attribute name="person" type="org.bibsonomy.model.Person" required="false"/>

	<link rel="stylesheet" href="${resdir}/jquery/plugins/ui/jquery-ui.css" type="text/css" media="all" />
	<link rel="stylesheet" href="${resdir}/jquery/plugins/ui/ui.theme.css" type="text/css" media="all" />

	<script type="text/javascript" src="${resdir}/javascript/publicationRequiredFields.js"> <!--  --></script>
	<script src="${resdir}/jquery/plugins/bgiframe/jquery.bgiframe.js" type="text/javascript"><!--  --></script>
	<script type="text/javascript">
		$(function() {
			$('#post\.resource\.bibtexKey').focus(function() {
				$(this).tooltip('toggle');
			});
		});
	</script>

	<spring:eval expression="T(org.bibsonomy.model.util.BibTexUtils).ENTRYTYPES" var="entryTypes" />

	<!-- entrytype help modal -->
	<bibtex:helpEntrtypes id="entrytypeHelp"/>
	<fmt:message key="post.resource.entrytype.help.title" var="entrytypeHelpTitle" />
	<fmt:message key="post.resource.entrytype.description" var="entrytypeHelpDescription" />

	<div class="form-group">
		<label class="col-sm-3 control-label" for="post.resource.entrytype">
			<fmt:message key="post.resource.entrytype" />
		</label>
		<div class="col-sm-9">
			<div class="row">
				<c:set var="selectColWidth" value="12"/>
				<c:if test="${empty hideBibTeXKey or not hideBibTeXKey}">
					<c:set var="selectColWidth" value="7"/>
				</c:if>

				<div class="col-md-${selectColWidth}">
					<select name="post.resource.entrytype" id="post.resource.entrytype" class="form-control" data-selected-entrytype="${post.resource.entrytype}">
						<c:forEach items="${entryTypes}" var="optionLabel" varStatus="cnt">
							<fmt:message key="post.resource.entrytype.${optionLabel}.title" var="messageString"/>
							<c:if test="${fn:startsWith(messageString,'???')}">
								<c:set var="messageString" value="${optionLabel}"/>
							</c:if>
							<fmt:message key="post.resource.entrytype.${optionLabel}.description" var="descriptionString"/>
							<c:if test="${fn:startsWith(descriptionString,'???')}">
								<c:set var="descriptionString" value=""/>
							</c:if>
							<option value="${entryTypes[cnt.index]}" data-description="${descriptionString}">
								<c:out value="${messageString}"/>
							</option>
						</c:forEach>
					</select>
				</div>
				<c:if test="${empty hideBibTeXKey or not hideBibTeXKey}">
					<div class="col-md-5">
						<div class="input-group">
							<fmt:message var="bibtexKey" key="post.resource.bibtexKey" />
							<fmt:message var="generateBibtexKey" key="post.resource.generateBibtexKey"/>
							<span class="input-group-btn">
								<a class="btn btn-default" title="${generateBibtexKey}" onclick="javascript:generateBibTexKey(this)">
									<span class="fa fa-refresh"><!--  --></span>
								</a>
							</span>
							<form:input placeholder="BibTeX key" id="post.resource.bibtexKey" path="post.resource.bibtexKey" cssClass="form-control" data-toggle="tooltip" data-placement="top" title="${bibtexKey}"/>
						</div><!-- /input-group -->
					</div>
				</c:if>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p class="help-block">
						<c:out value="${entrytypeHelpDescription} " escapeXml="${false}"/>
						<a href="#" class="" data-toggle="modal" data-target="#entrytypeHelp" title="${fn:escapeXml(entrytypeHelpTitle)}">
							<fmt:message key="post.resource.entrytype.help.learnMore" />
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>

	<bsform:textarea labelColSpan="3" id="post.resource.title" path="post.resource.title" required="${true}" />

	<bsform:textarea labelColSpan="3" id="post.resource.author" path="post.resource.author" required="${true}" />

	<c:if test="${person != null }">
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9">
			<!--
					<label style="margin-left: 0px; padding-left: 0px;">
					 -->
						<c:choose>
							<c:when test="${not empty person.personId}">
								<c:set var="personLink">
									<a href="${absoluteUrlGenerator.getPersonUrl(person.personId)}"><c:out value="${person.mainName.firstName} ${person.mainName.lastName}"/></a>
								</c:set>
								<fmt:message key="post.resource.person.associate.personIndex">
									<fmt:param value="${personLink}" />
								</fmt:message>
							</c:when>
							<c:otherwise>
								<fmt:message key="post.resource.person.new.personIndex" />
							</c:otherwise>
						</c:choose>

					<select name="personIndex" id="personIndex" style="display: inline-block; width: auto; margin-left: 1em;" class="form-control">
						<c:forEach begin="0" end="8" var="curPersonIndex">
							<c:choose>
								<c:when test="${curPersonIndex == personIndex}">
									<option value="${curPersonIndex}" selected="selected">${curPersonIndex + 1}</option>
								</c:when>
								<c:otherwise>
									<option value="${curPersonIndex}">${curPersonIndex + 1}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>

			</div>
			<fmt:message var="genealogyDefaultTag" key="person.newthesis.default.tag" />
			<input type="hidden" name="tags" value="${genealogyDefaultTag}" />
		</div>

		<c:if test="${((post.resource.entrytype eq 'phdthesis') or (post.resource.entrytype eq 'masterthesis'))}">
			<bsform:input labelColSpan="3" path="post.resource.school"/>
		</c:if>
	</c:if>

	<c:if test="${empty hideAuthorClaim or not hideAuthorClaim}">
		<spring:eval expression="T(org.bibsonomy.model.util.PersonNameUtils).serializePersonNames(T(org.bibsonomy.model.util.PersonNameUtils).discoverPersonNamesIgnoreExceptions('${command.context.loginUser.realname}'))" var="userRealname"/>
		<input type="hidden" id="userRealnameID" value="${userRealname}"/>
		<c:if test="${not empty command.context.loginUser.claimedPerson}">
			<spring:eval expression="T(org.bibsonomy.model.util.PersonNameUtils).serializePersonName('${command.context.loginUser.claimedPerson.mainName}')" var="claimedPersonMainName"/>
			<input type="hidden" id="claimedPersonMainNameID" value="${claimedPersonMainName}"/>
			<spring:eval expression="T(org.bibsonomy.model.util.PersonNameUtils).serializePersonNames('${command.context.loginUser.claimedPerson.names}', true, ' and ')" var="claimedPersonNames"/>
			<input type="hidden" id="claimedPersonNamesID" value="${claimedPersonNames}"/>
		</c:if>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9">
				<div class="checkbox clearfix">
					<label>
						<input name="myOwn" type="checkbox" class="help-popover" id="myownChkBox" tabindex="2" /> <fmt:message key="post.resource.myown"/>
						<div class="help-header hide"><fmt:message key="post.resource.myown"/></div>
						<div class="help-content hide"><fmt:message key="post.resource.myown.help"/></div>
					</label>
				</div>
				<div class="alert alert-warning alert-dismissible" role="alert" id="iAmAuthorWarning" style="display: none;">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<fmt:message key="post.resource.myown.reminder"/>
				</div>
			</div>
		</div>
	</c:if>

	<bsform:textarea labelColSpan="3" inputColSpan="9" id="post.resource.editor" path="post.resource.editor" required="${true}"/>
	<bsform:input labelColSpan="3" inputColSpan="9" id="post.resource.year" small="true" path="post.resource.year" required="${true}"/>
</jsp:root>