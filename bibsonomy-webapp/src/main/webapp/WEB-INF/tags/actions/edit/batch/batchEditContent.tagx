<?xml version="1.0" ?>
<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
		  xmlns:jsp="http://java.sun.com/JSP/Page"
		  xmlns:c="http://java.sun.com/jsp/jstl/core"
		  xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
		  xmlns:spring="http://www.springframework.org/tags"
		  xmlns:fn="http://java.sun.com/jsp/jstl/functions"
		  xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common"
		  xmlns:batch="urn:jsptagdir:/WEB-INF/tags/actions/edit/batch"
		  xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
		  xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
		  xmlns:discussion="urn:jsptagdir:/WEB-INF/tags/resources/discussion">

	<!--
		This page is called when the batch edit option is used for publications or bookmarks.
		It is strongly related to the importContent.tagx page, with the main difference being the list provided, so compatibility with that page must be ensured.
		 -->

	<jsp:directive.attribute name="listView" type="org.bibsonomy.webapp.command.ListCommand" required="true"/>
	<jsp:directive.attribute name="listViewStartParamName" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="otherPostsUrlPrefix" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="updateExistingPost" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="overwrite" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="directEdit" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="resourcetype" type="java.lang.String" required="true"
							 description="The type of resource that are edited (bookmark or bibtex)."/>

	<jsp:directive.attribute name="shortPostDescription" fragment="true" required="true"
							 description="A one line description of a post"/>

	<script type="text/javascript" src="${resdir}/javascript/batchEdit.js"><!-- keep me --></script>

	<!--=======================-->
	<!-- VARIABLEN DEKLARATION -->
	<!--=======================-->

	<c:if test="${empty updateExistingPost}">
		<c:set var="updateExistingPost" value="true"/>
	</c:if>

	<!-- OVERWRITE DUPLICATES -->
	<c:if test="${empty overwrite}">
		<c:set var="overwrite" value="false"/>
	</c:if>

	<!-- SUBMITBUTTON -->

	<fmt:message key="batchedit.button.update" var="buttonUpdate"/>
	<fmt:message key="batchedit.button.update" var="buttonUpdateViewable"/>
	<fmt:message key="batchedit.disregardedPosts.info" var="disregardedPosts"/>
	<!--=======================-->
	<!-- VARIABLEN DEKLARATION -->
	<!--=======================-->

	<jsp:directive.variable name-given="post" scope="NESTED"/>
	<form id="batchedit" action="/batchEdit" class="form-horizontal" method="post">
		<!-- the following variables are defined to communicate with .js file -->
		<input type="hidden" name="action"/>
		<input type="hidden" name="updateExistingPost" value="${updateExistingPost}"/>
		<input type="hidden" name="overwrite" value="${overwrite}"/>
		<input type="hidden" name="directEdit" value="${directEdit}"/>
		<input type="hidden" name="isImport" value="${false}"/>
		<input type="hidden" name="resourcetype" value="${resourcetype}"/>
		<input type="hidden" name="ckey" value="${ckey}" />
		<input type="hidden" name="referer"	value="${fn:escapeXml(command.referer)}" />

		<!-- Showing the edit Options -->
		<batch:batchOptions isImport="${false}" resourcetype="${resourcetype}"/>

		<!-- If there is no post to edit -->
		<c:if test="${empty listView.list}">
			<div class="alert alert-info col-sm-12" role="alert">
				<fmt:message key="batchedit.noPost.message"/>
			</div>
			<p class="clearfix"><!-- keep me --></p>
		</c:if>
		<!--  else.. -->
		<c:if test="${not empty listView.list}">
			<div class="row">
				<div class="col-md-6">
					<!-- we don't have pagination in indirect mode. -->
					<c:if test="${directEdit}">
						<div style="padding-left: 5px;">
							<rc:nextprev listView="${listView}" listViewStartParamName="${listViewStartParamName}"/>
							${mtl:ch('nbsp')}
						</div>
					</c:if>
				</div>
				<div class="col-md-2 col-md-offset-4">
					<c:set var="entriesPerPage" value="${resourcetype}.entriesPerPage"/>
					<c:set var="pageStart" value="${resourcetype}.start"/>
					<c:set var="requPathAndQuery" value="/${requPath}${requQueryString}"/>
					<c:set var="entriesSelectUrl" value="${mtl:removeParam(requPathAndQuery, pageStart)}"/>
					<select id="batchEditEntriesSelection" class="form-control pull-right" style="margin: 20px 0;">
						<option value="${mtl:setParam(entriesSelectUrl, entriesPerPage, listView.entriesPerPage)}">${listView.entriesPerPage}</option>
						<option value="${mtl:setParam(entriesSelectUrl, entriesPerPage, 5)}">5</option>
						<option value="${mtl:setParam(entriesSelectUrl, entriesPerPage, 10)}">10</option>
						<option value="${mtl:setParam(entriesSelectUrl, entriesPerPage, 20)}">20</option>
						<option value="${mtl:setParam(entriesSelectUrl, entriesPerPage, 100)}">100</option>
						<option value="${mtl:setParam(entriesSelectUrl, entriesPerPage, 500)}">500</option>
						<option value="${mtl:setParam(entriesSelectUrl, entriesPerPage, 1000)}">1000</option>
					</select>
				</div>
			</div>
			<table class="table table-striped" style="width: 100%; table-layout: fixed;">
				<thead>
					<tr>
						<th style="width: 40%;">
							<fmt:message key="batchedit.yourPosts"/>
						</th>
						<th id="yourTags" style="width: 30%;">
							<span style="padding-left: 12px; padding-right: 14px">
								<input type="checkbox" id="selectAllTags" value="true"
									   checked="checked"
									   title="select all"/>
							</span>
							<fmt:message key="batchedit.yourTags"/>
						</th>
						<c:if test="${resourcetype eq 'bibtex'}">
							<th  style="text-align: center; width: 10%; word-wrap: break-word;">
								<fmt:message key="batchedit.normalize"/>
							</th>
						</c:if>
						<th  style="text-align: center; width: 10%; word-wrap: break-word;">
							<fmt:message key="batchedit.updateVisibility"/>
						</th>
						<th  style="text-align: center; width: 10%; word-wrap: break-word;">
							<fmt:message key="batchedit.delete"/>
						</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="post" items="${listView.list}" varStatus="postCount">
					<div itemscope="itemscope" itemtype="http://schema.org/CreativeWork">

						<!-- the following variables are defined to communicate with .js file -->
						<input type="hidden" name="posts['${post.resource.intraHash}_${post.user.name}'].intraHash"
							   value="${post.resource.intraHash}"/>
						<input type="hidden" name="posts['${post.resource.intraHash}_${post.user.name}'].postOwner"
							   value="${post.user.name}"/>

						<!-- Check if the post has any kind of error connected to it -->
						<spring:bind path="command.${resourcetype}.list[${postCount.index}].resource.*">
							<c:set var="postHasError" value="${status.error}"/>
						</spring:bind>
						<c:if test="${not postHasError}">
							<spring:bind path="command.${resourcetype}.list[${postCount.index}].resource">
								<c:set var="postHasError" value="${status.error}"/>
							</spring:bind>
						</c:if>
						<c:if test="${not empty postsErrorMessages[post.resource.intraHash]}">
							<c:set var="postHasError" value="${true}"/>
						</c:if>

						<c:choose>
							<!-- If the post has any errors it will only show in a disabled view without any of the batchEdit controls -->
							<c:when test="${postHasError}">
								<c:choose>
									<c:when test="${post.user.name eq command.context.loginUser.name}">
										<!-- Determine the tooltip-message based on the given error -->
										<spring:bind
												path="command.${resourcetype}.list[${postCount.count - 1}].resource.*">
											<c:forEach var="errorMessage" items="${status.errorMessages}"
													   varStatus="stat">
												<c:set var="tooltip_msg"
													   value="${stat.first ? '' : tooltip_msg} ${stat.count}. ${errorMessage}"/>
											</c:forEach>
										</spring:bind>
										<spring:bind
												path="command.${resourcetype}.list[${postCount.count - 1}].resource">
											<c:forEach var="errorMessage" items="${status.errorMessages}"
													   varStatus="stat">
												<c:set var="tooltip_msg"
													   value="${stat.first ? '' : tooltip_msg} ${stat.count}. ${errorMessage}"/>
											</c:forEach>
										</spring:bind>
										<c:if test="${not empty postsErrorMessages[post.resource.intraHash]}">
											<c:forEach var="errorMessage"
													   items="${postsErrorMessages[post.resource.intraHash]}"
													   varStatus="stat">
												<c:set var="tooltip_msg"
													   value="${stat.first ? '' : tooltip_msg} ${stat.count}. ${errorMessage}"/>
											</c:forEach>
										</c:if>

										<batch:batchEditTableEntry
												post = "${post}"
												isImport="${false}"
												postListIndex="${postCount.count - 1}"
												resourcetype="${resourcetype}"
												hasErrors="${true}"
												tooltip_msg="${tooltip_msg}">
												<jsp:attribute name="shortPostDescription">
													<jsp:invoke fragment="shortPostDescription" />
												</jsp:attribute>
										</batch:batchEditTableEntry>

									</c:when>
									<c:otherwise>
										<!-- remember, that we have found posts not owned by the user to show the corresponing info text -->
										<c:set var="otherPosts" value="true"/>
									</c:otherwise>
								</c:choose>
							</c:when>

							<c:otherwise>
								<c:set var="canEdit" value="${post.user.name eq command.context.loginUser.name }"/>
								<c:forEach items="${command.context.loginUser.groups}" var="group">
									<c:if test="${group.name eq post.user.name}">
										<c:set var="membership"
											   value="${group.getGroupMembershipForUser(command.context.loginUser.name) }"/>
										<c:if test="${not empty membership and (membership.groupRole eq 'MODERATOR' or membership.groupRole eq 'ADMINISTRATOR') }">
											<c:set var="canEdit" value="${true}"/>
										</c:if>
									</c:if>
								</c:forEach>
								<c:choose>
									<!--  a post owned by the logged in user - show a tag edit form -->
									<c:when test="${canEdit}">
										<batch:batchEditTableEntry
												post="${post}"
												isImport="${false}"
												postListIndex="${postCount.count - 1}"
												resourcetype="${resourcetype}">
												<jsp:attribute name="shortPostDescription">
													<jsp:invoke fragment="shortPostDescription" />
												</jsp:attribute>
										</batch:batchEditTableEntry>
									</c:when>
									<c:otherwise>
										<batch:batchEditTableEntry
												post="${post}"
												isImport="${false}"
												postListIndex="${postCount.count - 1}"
												resourcetype="${resourcetype}"
												isDisabled="${true}"
												tooltip_msg="${disregardedPosts}">
												<jsp:attribute name="shortPostDescription">
													<jsp:invoke fragment="shortPostDescription" />
												</jsp:attribute>
										</batch:batchEditTableEntry>
										<!-- remember, that we have found posts not owned by the user to show the corresponing info text -->
										<c:set var="otherPosts" value="true"/>
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
					</div>
				</c:forEach>

				<!-- if there exists some posts which are not owned by the user -->
				<c:if test="${not empty otherPosts}">
					<c:set var="tooltip_msg" value="${disregardedPosts}"/>
					<tr>
						<div class="alert alert-info col-sm-10" style="margin-bottom:0px;" role="alert">
							<fmt:message key="batchedit.disregardedPosts.alert"/>
						</div>
					</tr>
				</c:if>
				</tbody>
			</table>
		</c:if>
	</form>
</jsp:root>