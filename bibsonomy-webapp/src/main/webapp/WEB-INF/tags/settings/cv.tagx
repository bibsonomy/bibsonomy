<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld">

	<jsp:directive.attribute name="command" type="org.bibsonomy.webapp.command.BaseCommand" required="true" />
	<jsp:directive.attribute name="isGroup" required="false" />
	<c:if test="${empty isGroup}">
		<c:set var="isGroup" value="${command.isGroup}" />
	</c:if>

	<fmt:message key="cv.layout.name.def1en" var="def1en" />
	<fmt:message key="cv.layout.name.def1ru" var="def1ru" />
	<fmt:message key="cv.layout.name.def1ger" var="def1ger" />
	<fmt:message key="cv.layout.name.def2en" var="def2en" />
	<fmt:message key="cv.layout.name.def2ru" var="def2ru" />
	<fmt:message key="cv.layout.name.def2ger" var="def2ger" />
	<fmt:message key="cv.layout.name.curr" var="curr" />
	<fmt:message key="cv.layout.name.clear" var="clear" />
	<fmt:message key="cv.customlayout" var="customlayout" />
	<fmt:message key="cv.defaultlayout" var="defaultlayouts" />
	<fmt:message key="cv.heading" var="heading" />
	<fmt:message key="cv.admin.layout" var="layout" />
	<fmt:message key="cv.admin.preview" var="preview" />
	<fmt:message key="cv.admin.preview_pub" var="pPrev" />
	<fmt:message key="cv.admin.save" var="save" />
	<fmt:message key="cv.admin.hide" var="hide" />
	<fmt:message key="cv.admin.show" var="show" />

	<div id="selTab5" class="tab-pane ${(command.selTab eq 5) ? 'active' : ''}">

		<fieldset id="fsOuter">
		<div class="form-group">
			<bsform:fieldset legendKey="cv.heading">
				<jsp:attribute name="content">
					<c:choose>
						<c:when test="${isGroup}">
							<p><fmt:message key="cv.admin.topic" >
								<fmt:param>/cv/group/${fn:escapeXml(command.group.name)}</fmt:param>
							</fmt:message></p>
						</c:when>

						<c:otherwise>
							<p><fmt:message key="cv.admin.topic" >
								<fmt:param>/cv/user/${fn:escapeXml(command.context.loginUser.name)}</fmt:param>
							</fmt:message></p>
						</c:otherwise>
					</c:choose>
				</jsp:attribute>
			</bsform:fieldset>
		</div>
		<div class="form-group">
			<bsform:fieldset legendKey="cv.admin.show">
				<jsp:attribute name="content">
					<form:form class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-3 control-label">choose a layout</label>
							<div class="col-sm-9">
								<c:choose>
									<c:when test="${isGroup}">
										<form:select id="changeLayout" path="" cssClass="form-control">
											<form:option data-layout='LAYOUT_CURRENT' value="${curr}">${curr}</form:option>
											<form:option data-layout="LAYOUT_G_DEFAULT_I_EN" value="${def1en}">${def1en}</form:option>
											<form:option data-layout="LAYOUT_G_DEFAULT_I_GER" value="${def1ger}">${def1ger}</form:option>
											<form:option data-layout="LAYOUT_G_DEFAULT_I_RU" value="${def1ru}">${def1ru}</form:option>
										</form:select>
									</c:when>

									<c:otherwise>
										<form:select id="changeLayout" path="" cssClass="form-control">
											<form:option data-layout='LAYOUT_CURRENT' value="${curr}">${curr}</form:option>
											<form:option data-layout='LAYOUT_DEFAULT_I_EN' value="${def1en}">${def1en}</form:option>
											<form:option data-layout='LAYOUT_DEFAULT_II_EN' value="${def2en}">${def2en}</form:option>
											<form:option data-layout='LAYOUT_DEFAULT_I_GER' value="${def1ger}">${def1ger}</form:option>
											<form:option data-layout='LAYOUT_DEFAULT_II_GER' value="${def2ger}">${def2ger}</form:option>
											<form:option data-layout='LAYOUT_DEFAULT_I_RU' value="${def1ru}">${def1ru}</form:option>
											<form:option data-layout='LAYOUT_DEFAULT_II_RU' value="${def2ru}">${def2ru}</form:option>
										</form:select>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<input type='hidden' name='ckey' id='ckey' value="${fn:escapeXml(command.context.ckey)}" />
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9" id="saveButtons">
								<div class="btn-group btn-group-sm">
									<bsform:button size="defaultSize" id="saveButton" onclick="return submitWiki('save');" value="${save}" style="primary" />
									<bsform:button size="defaultSize" id="showAdmin" value="${show}" style="defaultStyle" />
									<button type="button" id="hideAdmin" class="btn btn-default" style="display:none"><c:out value="${hide}" /></button>
								</div>
							</div>
							<div class="col-md-4" id="statusField">
								<div style="float:right; display: none" class="col-md-2"> 
									<span id="loadingDiv">
										<fontawesome:icon icon="circle-o-notch" fixedWidth="${true}" spin="${true}"/>
									</span>
								</div>
								<div id="statusText" class="col-md-10"><!-- keep me --></div>
							</div>
						</div>
						
						<!-- Text edit area -->
						<div style="display: none" id="wikiEditArea">
							<bsform:textarea labelColSpan="3" inputColSpan="9" id="wikiTextArea" path="wikiText" labelKey="cv.editCV" noHelp="true" rows="10" />
							<div class="form-group">
								<div id="editButtons" class="col-sm-offset-3 col-sm-9 btn-group btn-group-sm">
									<button type="button" onclick="return submitWiki('preview');" class="btn btn-default">
										<c:out value="${preview}" />
									</button>
									<button type="button" onclick="return submitWiki('publicPreview');" class="btn btn-default">
										<c:out value="${pPrev}" />
									</button>
									<button type="button" onclick="return clearCVTextField();" class="btn btn-sm btn-danger">
										<c:out value="${clear}" />
									</button>
								</div>

							</div>
						</div>

					</form:form>
				</jsp:attribute>
			</bsform:fieldset>
			</div>
			<p class="clearfix"><!--  --></p>

			<!--+
				| Preview Area
				+-->
			<bsform:fieldset legendKey="cv.preview">
				<jsp:attribute name="content">
					<div id="wikiArea">
						<c:choose>
							<c:when test="${not empty command.wikiText}">
								<c:out value="${command.renderedWikiText}" escapeXml="false" />
							</c:when>
							<c:otherwise>
								<fmt:message key="cv.my.nowikitext" var="nowikitext" />
								<c:out value="${nowikitext}" escapeXml="false" />
							</c:otherwise>
						</c:choose>
					</div>
				</jsp:attribute>
			</bsform:fieldset>
		</fieldset> <!-- fsOuter -->
	</div> <!-- /#selTab5 -->

</jsp:root>