<jsp:root version="2.0"
  xmlns="http://www.w3.org/1999/xhtml"  
  xmlns:jsp="http://java.sun.com/JSP/Page"
  xmlns:c="http://java.sun.com/jsp/jstl/core"
  xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
  xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
  xmlns:fn="http://java.sun.com/jsp/jstl/functions"
  xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
  xmlns:form="http://www.springframework.org/tags/form"
  xmlns:spring="http://www.springframework.org/tags"
  xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
  xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
  xmlns:settings="urn:jsptagdir:/WEB-INF/tags/settings"
  xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
  xmlns:bs="urn:jsptagdir:/WEB-INF/tags/bs"
  xmlns:export="urn:jsptagdir:/WEB-INF/tags/export/bibtex">
  
	<jsp:directive.attribute name="command"  type="org.bibsonomy.webapp.command.BaseCommand" required="true"/>
	
  
	<!-- ############################################# settings tab ############################################# -->
	<fmt:message var="cloud" key="settings.cloud"/>
	<fmt:message var="list" key="settings.list"/>
	<fmt:message var="alphabet" key="settings.alphabet"/>
	<fmt:message var="frequency" key="settings.frequency"/>
	<fmt:message var="yes" key="settings.yes"/>
	<fmt:message var="no" key="settings.no"/>
	<fmt:message var="setlayout" key="settings.setlayout"/>
	<fmt:message var="generateapikey" key="settings.generateapikey"/>
	<fmt:message var="updatesettings" key="settings.updatesettings" />
	<fmt:message var="deleteaccount" key="settings.deleteaccount" />
	<fmt:message var="all" key="settings.all"/>
	<fmt:message var="bookmark" key="settings.bookmark"/>
	<fmt:message var="publication" key="settings.publication"/>
	<fmt:message var="advanced" key="settings.advanced"/>
	<fmt:message var="simple" key="settings.simple"/>

	<div id="selTab1" class="tab-pane ${(command.selTab eq 1) ? 'active' : ''}">
		
	
		<!-- layout of your tag box and post lists -->

		<fieldset>
			<form:form cssClass="form-horizontal" method="POST" action="/updateUserSettings">
				<fieldset>
					<legend><fmt:message key="settings.styleSettingsHeader" /></legend>
					<!-- show tags as -->
					<!-- new -->
					<div class="form-group">
					
						<form:label cssClass="col-sm-3 control-label" path="user.settings.tagboxStyle">
							<fmt:message key="settings.tag.showAs"/>
						</form:label>
						
						<div class="col-sm-9">
							<form:select cssClass="form-control help-popover" path="user.settings.tagboxStyle" >
								<form:option label="${cloud}" value="0"/>
								<form:option label="${list}" value="1"/>
							</form:select >
							
							<div class="help help-header hide"><fmt:message key="settings.tag.showAs"/></div>
							<div class="help help-content hide">
								<p><fmt:message key="settings.tag.showAs.help"/></p>
							</div>
						</div>
						
					</div>
					
					<!-- sort tags by -->
					<div class="form-group">
					
						<form:label cssClass="col-sm-3 control-label" path="user.settings.tagboxSort">
							<fmt:message key="settings.tag.sortBy"/>
						</form:label>
						<div class="col-sm-9">
							<form:select cssClass="form-control help-popover" path="user.settings.tagboxSort" >
								<form:option label="${alphabet}" value="0"/>
								<form:option label="${frequency}" value="1"/>
							</form:select >
							
							<div class="help help-header hide"><fmt:message key="settings.tag.sortBy"/></div>
							<div class="help help-content hide">
								<p><fmt:message key="settings.tag.sortBy.help"/></p>
							</div>
						</div>
						
					</div>

					
					<!-- tooltips for tags -->
					<div class="form-group">
					
						<form:label cssClass="col-sm-3 control-label" path="user.settings.tagboxTooltip">
							<fmt:message key="settings.tag.showTooltipsForTags"/>
						</form:label>
						
						<div class="col-sm-9">
							<form:select cssClass="form-control help-popover" path="user.settings.tagboxTooltip" >
								<form:option label="${no}" value="0"/>
								<form:option label="${yes}" value="1"/>
							</form:select >
							
							<div class="help help-header hide"><fmt:message key="settings.tag.showTooltipsForTags"/></div>
							<div class="help help-content hide">
								<p><fmt:message key="settings.tag.showTooltipsForTags.help"/></p>
							</div>
							
						</div>
						
					</div>
					
					<!-- choice of tags -->
					<div class="form-group">
					
						<form:label cssClass="col-sm-3 control-label" path="user.settings.isMaxCount">
							<fmt:message key="settings.tag.maxCountminFreq"/>
						</form:label>
						
						<fmt:message var="maxCount" key="settings.tag.options.maxCount" />
						<fmt:message var="minFreq" key="settings.tag.options.minFreq" />
						
						<div class="col-sm-9">
							<form:select cssClass="form-control help-popover" path="user.settings.isMaxCount" >
								<form:option label="${maxCount}" value="true"/>
								<form:option label="${minFreq}" value="false"/>
							</form:select >
							
							<div class="help help-header hide"><fmt:message key="settings.tag.maxCountminFreq"/></div>
							<div class="help help-content hide">
								<p><fmt:message key="settings.tag.maxCountminFreq.help"/></p>
								<ol>
									<li>
										<fmt:message key="settings.tag.maxCountminFreq.help.maxCount">
											<fmt:param>${maxCount}</fmt:param>
										</fmt:message>
									</li>
									<li>
										<fmt:message key="settings.tag.maxCountminFreq.help.minFreq">
											<fmt:param>${minFreq}</fmt:param>
										</fmt:message>
									</li>
								</ol>
								<p><fmt:message key="settings.tag.maxCountminFreq.help.X" /></p>
							</div>
							
						</div>
						
					</div>
					
					<!-- tag threshold -->

					<fmt:message var="labelText" key="settings.tag.threshold" />
					<c:set var="helpText">
					<fmt:message key="settings.tag.threshold.help"/>
						<ol>
								<li>
									<fmt:message key="settings.tag.threshold.help.maxCount">
										<fmt:param>${maxCount}</fmt:param>
									</fmt:message>
								</li>
								<li>
									<fmt:message key="settings.tag.threshold.help.minFreq">
										<fmt:param>${minFreq}</fmt:param>
									</fmt:message>
								</li>
						</ol>
					</c:set>
					<bsform:input path="changeTo" labelColSpan="3" labelText="${labelText }" inputColSpan="9" helpText="${helpText }" />
					
					<!-- items per page -->
					<!-- new -->
					<fmt:message var="labelText" key="settings.tag.itemsPerPage" />
					<fmt:message var="helpText" key="settings.tag.itemsPerPage.help"/>
					<bsform:input path="user.settings.listItemcount" labelColSpan="3"  labelText="${labelText}" inputColSpan="9" helpText="${helpText }" />
					<div class="dissError">
						<form:errors path="user.settings.listItemcount" />
					</div>
					
					
					<!-- favourite layouts to be shown-->
					<div class="form-group">
						<form:label cssClass="col-sm-3 control-label" path="user.settings.favouriteLayouts">
							<fmt:message key="settings.favouriteExports" />
						</form:label>
					
						<!-- shown style dropdown list including a nice and smooth animation -->
						<div class="col-sm-9">
							<ul class="list-group" id="favouriteLayoutsList">
								<!-- ID has to be "source"/"id" for the StringToFavouriteLayoutConverter to read -->
								<!-- don't forget to use displayName in the right places! -->
								<c:forEach var="favl" items="${command.context.loginUser.settings.favouriteLayouts}" varStatus="counter">
									<li class="list-group-item favouriteLayoutsListItem clearfix" data-source="${favl.source}" data-style="${favl.style}">
										<input type="hidden" name="user.settings.favouriteLayouts" id="${favl.source}/${favl.style}" value="${favl.source}/${favl.style}"/>
										<mtl:favouriteLayoutDisplayName favouriteLayout="${favl}" />
										<c:out value=" " /> <!-- TODO: use css -->
										<span class="btn btn-danger btn-xs pull-right delete-Style">
											<fmt:message key="delete" />
										</span>
									</li>
								</c:forEach>
							</ul>

							<!-- add new styles with typeahead -->
							<div class="row">
								<div class="col-sm-3 text-right">
									<fmt:message key="settings.favouriteExports.add" />
								</div>
								<div class="col-sm-9">
									<settings:favouriteLayoutSelect />
								</div>
							</div>
						</div>
					</div>

					<!-- displayed resources -->
					<!-- new -->
					<div class="form-group">
						<form:label cssClass="col-sm-3 control-label" path="user.settings.showBookmark">
							<fmt:message key="settings.resource.resourceToShow"/>
						</form:label>
						
						<div class="col-sm-3">
							<label>
								<form:checkbox path="user.settings.showBookmark" />&amp;nbsp;
								<fmt:message key="settings.bookmark"/>
							</label>
						</div>
						<div class="col-sm-5">
							<label>
								<form:checkbox path="user.settings.showBibtex" />&amp;nbsp;
								<fmt:message key="settings.publication"/>
							</label>
						</div>
						<div class="dissError"><form:errors path="user.settings.showBookmark" /></div>
					</div>
					
					<!-- appearance -->
					<div class="form-group">
						<form:label cssClass="col-sm-3 control-label" path="user.settings.layoutSettings.simpleInterface">
							<fmt:message key="settings.editPublication.interFace"/>
						</form:label>
						
						<fmt:message var="maxCount" key="settings.tag.options.maxCount" />
						<fmt:message var="minFreq" key="settings.tag.options.minFreq" />
						
						<div class="col-sm-9">
							<form:select cssClass="form-control help-popover" path="user.settings.layoutSettings.simpleInterface" >
								<form:option label="${advanced}" value="false"/>
								<form:option label="${simple}" value="true"/>
							</form:select>
							
							<div class="help help-header hide"><fmt:message key="settings.editPublication.interFace"/></div>
							<div class="help help-content hide">
								<p><fmt:message key="settings.editPublication.interFace.help"/></p>
								<ol>
									<li> <fmt:message key="settings.editPublication.interFace.help.advanced">
											<fmt:param>${advanced}</fmt:param>
										</fmt:message>
									</li>
									<li> <fmt:message key="settings.editPublication.interFace.help.simple">
											<fmt:param>${simple}</fmt:param>
										</fmt:message>
									</li>
								</ol>
							</div>
						</div>
					</div>
					
					<!-- language -->
					<div class="form-group">
						<form:label cssClass="col-sm-3 control-label" path="user.settings.defaultLanguage">
							<fmt:message key="settings.tag.defaultLanguage"/>
						</form:label>
						
						<div class="col-sm-9">
							<form:select cssClass="form-control help-popover" path="user.settings.defaultLanguage" >
								<c:set var="supportedLocales" value="${fn:split(properties['system.locales.supported'],',')}"/>
								<c:forEach var="lang" items="${supportedLocales}" varStatus="status">
									<fmt:message var="langLabel" key="settings.locales.${lang}" />
									<form:option label="${langLabel}" value="${lang}"/>
								</c:forEach>
							</form:select >
							
							<div class="help help-header hide"><fmt:message key="settings.tag.defaultLanguage"/></div>
							<div class="help help-content hide">
								<p><fmt:message key="settings.tag.defaultLanguage.help"/></p>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<input type="hidden" name="ckey" value="${ckey}"/>
						<input type="hidden" name="action" value="layoutTagPost"/>
						<input type="hidden" name="selTab" value="1"/>
						
						<div class="col-sm-offset-3 col-sm-9">
							<bsform:button type="submit" style="primary" size="defaultSize"  value="${setlayout}" />
						</div>
					</div>
				</fieldset>
			</form:form>
					
			<form:form cssClass="form-horizontal" id="postForm" method="post" action="/updateUserSettings">
				<fieldset>
					<legend>API</legend>
					<!-- show API key -->
					<c:choose>
						<c:when test="${command.context.loginUser.apiKey eq null}">
							<p>
								<fmt:message key="settings.apisupport"><fmt:param>${properties['project.apiemail']}</fmt:param></fmt:message>
							</p> 
						</c:when>
						<c:otherwise>
						
							<div class="form-group">
								<label class="col-sm-3 control-label"><fmt:message key="settings.yourapikey"/></label>
								<div class="col-sm-9">
									<p class="form-control-static"><tt><c:out value="${command.context.loginUser.apiKey}" /></tt></p>
								</div>
								<div class="col-sm-offset-3 col-sm-9">
									<p class="help-block">
										<fmt:message key="settings.questionsregardingapi">
											<fmt:param>
												<span style="white-space: pre-wrap;"><a href="https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API"><fmt:message key="settings.apidoc"/></a></span>
											</fmt:param>
											<fmt:param>
												<!-- TODO: make this configurable -->
												<span style="white-space: nowrap;"><parts:email address="${properties['project.apiemail']}"/></span>
											</fmt:param>
										</fmt:message>
									</p>
								</div>
							</div>
							<div class="form-group">
								<input type="hidden" name="selTab" value="1" />
								<input type="hidden" name="ckey" value="${ckey}" />
								<input type="hidden" name="action" value="api" />

								<div class="col-sm-offset-3 col-sm-9">
									<bsform:button type="submit" style="primary" size="defaultSize" value="${generateapikey}" />
								</div>
							</div>
						</c:otherwise>
					</c:choose>
				</fieldset>
			</form:form>
					
			<form:form class="form-horizontal" id="postForm" method="post" action="/updateUserSettings">
				<fieldset>
					<legend><fmt:message key="settings.logging" /></legend>	
							
					<!-- allow click logging -->
					<div class="form-group">
							
						<form:label cssClass="col-sm-3 control-label" path="user.settings.logLevel">
							<fmt:message key="settings.logclicks"/>
						</form:label>
								
						<fmt:message var="yes" key="settings.yes" />
						<fmt:message var="no" key="settings.no" />
								
						<div class="col-sm-9">
							<form:select cssClass="form-control help-popover" path="user.settings.logLevel" >
								<form:option label="${yes}" value="0"/>
								<form:option label="${no}" value="1"/>
							</form:select >
							
							<div class="help help-header hide"><fmt:message key="settings.logclicks"/></div>
							<div class="help help-content hide">
								<fmt:message key="settings.logclicks.help"/>
							</div>
						</div>
					</div>
					
					<!-- confirmation before deleting -->
					<div class="form-group">
						<form:label cssClass="col-sm-3 control-label" path="user.settings.confirmDelete">
							<fmt:message key="settings.confirm"/>
						</form:label>
						
						<fmt:message var="yes" key="settings.yes" />
						<fmt:message var="no" key="settings.no" />
						
						<div class="col-sm-9">
							<form:select cssClass="form-control help-popover" path="user.settings.confirmDelete" >
								<form:option label="${yes}" value="true"/>
								<form:option label="${no}" value="false"/>
							</form:select >
							<div class="help help-header hide"><fmt:message key="settings.confirm"/></div>
							<div class="help help-content hide">
								<fmt:message key="settings.confirm.help"/>
							</div>
						</div>
					</div>
					<div class="form-group">
						<input type="hidden" name="ckey" value="${ckey}"/>
						<input type="hidden" name="selTab" value="1"/>
						<input type="hidden" name="action" value="logging"/>
						<div class="col-sm-offset-3 col-sm-9">
							<bsform:button type="submit" style="primary" size="defaultSize" value="${updatesettings}" />
						</div>
					</div>
				</fieldset>
			</form:form>
					
			<!-- show password change dialog only if internal authentication is configured -->
		
			<c:set var="openIdOrLDAPUser" value="${not empty command.context.loginUser.openID or not empty command.context.loginUser.ldapId}" />
			<c:if test="${mtl:contains(authConfig, mtl:convertToEnum('org.bibsonomy.common.enums.AuthMethod', 'internal')) and not openIdOrLDAPUser}">
				<settings:passwordChange loginUser="${command.context.loginUser}"/>
			</c:if>
			
			<form:form cssClass="form-horizontal" id="postForm" method="post" action="/actions/goodBye">
				<fieldset>
					<legend><fmt:message key="settings.deletemyaccount" /></legend>
					<!-- Are you sure? -->
					<fmt:message var="labelText" key="settings.really" />
					<bsform:input path="delete" placeholder="no" labelColSpan="3" inputColSpan="9" noHelp="true" labelText="${labelText }" />
					
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-9">
							<p class="help-block">
								<fmt:message key="settings.yestodeleteaccount">
									<fmt:param>
										<em>yes</em>
									</fmt:param>
								</fmt:message>
							</p>
							
							<input type="hidden" name="ckey" value="${command.context.ckey}"/>
							<input type="hidden" name="selTab" value="1"/>
							<bsform:button type="submit" style="danger" size="defaultSize" value="${deleteaccount}" />
						</div>
					</div>
			</fieldset>
		</form:form>
	</fieldset>
	</div>
</jsp:root>