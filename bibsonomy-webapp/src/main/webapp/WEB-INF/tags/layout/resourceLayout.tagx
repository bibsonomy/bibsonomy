<jsp:root version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:resources="urn:jsptagdir:/WEB-INF/tags/resources"
	xmlns:publication="urn:jsptagdir:/WEB-INF/tags/resources/publication"
	xmlns:goldstandardpublication="urn:jsptagdir:/WEB-INF/tags/resources/goldstandardpublication"
	xmlns:goldstandardbookmark="urn:jsptagdir:/WEB-INF/tags/resources/goldstandardbookmark"
	xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:export="urn:jsptagdir:/WEB-INF/tags/export/bibtex"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld">
	
	<jsp:directive.attribute name="command" type="org.bibsonomy.webapp.command.SimpleResourceViewCommand" required="true"/>	
	
	<jsp:directive.attribute name="requPath" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="requQueryString" type="java.lang.String" required="false"/>
	<jsp:directive.attribute name="pageTitle" type="java.lang.String" required="true"/>
	
	<jsp:directive.attribute name="disableListNavigation" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="disableResourcetypeSelection" type="java.lang.Boolean" required="false"/>

	<jsp:directive.attribute name="activeTab" type="java.lang.String" required="false"/>

	<jsp:directive.attribute name="isClipboard" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="showPageOptions" type="java.lang.Boolean" required="false"/>
	
	<jsp:directive.attribute name="customPageOptions" fragment="true" required="false"/>
	<jsp:directive.attribute name="customBookmarksHeaderText" type="java.lang.String" required="false"/>
	<jsp:directive.attribute name="customPublicationsHeaderText" type="java.lang.String" required="false"/>
	
	<jsp:directive.attribute name="noSidebar" type="java.lang.Boolean" required="false"/>
	
	<jsp:directive.attribute name="heading" fragment="true" required="true"/>
	<jsp:directive.attribute name="infobox" fragment="true" required="false"/>
	<jsp:directive.attribute name="sidebar" fragment="true" required="false"/>
	<jsp:directive.attribute name="newsbar" fragment="true" required="false"/>	
	<jsp:directive.attribute name="contentHeading" fragment="true" required="false"/>
	<jsp:directive.attribute name="headerExt" fragment="true" required="false"/>
	<jsp:directive.attribute name="discussion" fragment="true" required="false"/>
	<jsp:directive.attribute name="content" fragment="true" required="false" />
	<jsp:directive.attribute name="breadcrumbs" fragment="true" required="false" description="Breadcrumb navigation"/>

	<jsp:directive.attribute name="sortPage" type="java.lang.String" required="false" />
	<jsp:directive.attribute name="showSortInfoHeadline" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="additionalSorting" fragment="true"/>
	<!--+
		|
		| variables & messages
		|
		+-->
	<fmt:message key="feeds.bookmark" var="bookmarkRssFeed"/>
	
	<!--+
		| which resource types to display (controlled via URL-param, format, user settings; see ResourceListController.getListsToInitialize)
		+ -->
	<c:set var="showPublication" value="${(mtl:containsResourceClass(command.resourcetype, 'publication') or mtl:containsResourceClass(command.resourcetype, 'all')) and empty command.goldStandardPublications.list }" />
	<c:set var="showBookmark" value="${mtl:containsResourceClass(command.resourcetype, 'bookmark') or mtl:containsResourceClass(command.resourcetype, 'all')}" />
	<c:set var="bookmarksWidthClass" value="${showPublication and showBookmark ? 'standard' : (showBookmark ? 'wide' : 'narrow')}"/>
	<c:set var="publicationsWidthClass" value="${showPublication and showBookmark ? 'standard' : (showPublication ? 'wide' : 'narrow')}"/>
	<c:set var="excapedRequestPath" value="${fn:escapeXml(requPath)}" />
	
	<!--+
		+ main layout
		+-->	
	<layout:layout loginUser="${command.context.loginUser}"
			pageTitle="${pageTitle}" heading="${heading}"
			requestedUser="${command.requestedUser}"
			infobox="${infobox}"
			sidebar="${sidebar}"
			noSidebar="${noSidebar}"
			newsbar="${newsbar}"
			requPath="${requPath}"
			activeTab="${activeTab}"
			personalized="${command.personalized}"
			breadcrumbs="${breadcrumbs}">
			
		<!--+
			+ HTML header extension
			+ -->
		<jsp:attribute name="headerExt">
			<c:if test="${showPublication}">
				<publication:feeds path="/${fn:escapeXml(requPath)}" />
			</c:if>
			<c:if test="${showBookmark}">
				<link rel="alternate" type="application/rss+xml" title="${bookmarkRssFeed} /${excapedRequestPath}" href="/rss/${excapedRequestPath}" />
			</c:if>
			
			<!-- UnAPI service -->
			<link rel="unapi-server" type="application/xml" title="unAPI" href="${properties['project.home']}unapi"/>

			<script type="text/javascript" src="/resources/javascript/resourceLayout.js"><!-- keep me --></script>

			<jsp:invoke fragment="headerExt"/>
		</jsp:attribute>
		
		<!--+
			+ main content (resource lists)
			+-->
		<jsp:attribute name="content">
			<jsp:invoke fragment="content" />
			<!-- additional heading for content area -->
			<c:if  test="${not empty contentHeading}">
				<jsp:invoke fragment="contentHeading" />
			</c:if>
			
			<c:set var="all" value="${mtl:containsResourceClass(command.resourcetype, 'all')}" />
			<c:set var="bm" value="${mtl:containsResourceClass(command.resourcetype, 'bookmark')}" />
			<c:set var="pub" value="${mtl:containsResourceClass(command.resourcetype, 'publication')}" />
			
			<export:modal />

			<!-- bibtex/bookmark lists -->
			<div class="row">
				<div class="${ (bm and pub) ? 'col-md-6' : ( (bm and not pub) ? 'col-md-12' : ((not bm) ? 'hide' : ''))} bookmark-list">
					
					<resources:bookmarks listView="${command.bookmark}" 
						loginUserName="${command.context.loginUser.name}"
						disableListNavigation="${disableListNavigation}"
						requPath="${excapedRequestPath}" 
						widthClassName="${bookmarksWidthClass}"
						isClipboard="${isClipboard}"
						showBookmark="${showBookmark}"
						showPublication="${showPublication}"
						additionalSorting="${additionalSorting}">
					
						<jsp:attribute name="addAsLastItem">
							<c:if test="${not disableListNavigation}">
								<div id="bookmarksfooter" class="${bookmarksWidthClass}">
									<rc:nextprev listView="${command.bookmark}" listViewStartParamName="bookmark.start"/>
								</div>
							</c:if>
						</jsp:attribute>
					</resources:bookmarks>
				</div>
				<div class="${ (pub and bm) ? 'col-md-6' : ( (pub and not bm) ? 'col-md-12' : ((not pub) ? 'hide' : ''))} publication-list">
					<resources:bibtexs listView="${command.bibtex}" 
							loginUserName="${command.context.loginUser.name}" 
							disableListNavigation="${disableListNavigation}" 
							requPath="${excapedRequestPath}"
							requQueryString="${requQueryString}"
							widthClassName="${publicationsWidthClass}"
							isClipboard="${isClipboard}"
							showBookmark="${showBookmark}"
							showPublication="${showPublication}"
						    sortPage="${sortPage}"
							showSortInfoHeadline="${showSortInfoHeadline}"
							additionalSorting="${additionalSorting}">
						<jsp:attribute name="addAsLastItem">
							<c:if test="${not disableListNavigation}">
								<div id="publicationsfooter" class="${publicationsWidthClass}">
									<rc:nextprev listView="${command.bibtex}" listViewStartParamName="bibtex.start"/>
								</div>
							</c:if>
						</jsp:attribute>
					</resources:bibtexs>
				</div>
			</div>
		</jsp:attribute>
	</layout:layout>
</jsp:root>
