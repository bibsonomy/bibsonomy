<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
          xmlns:jsp="http://java.sun.com/JSP/Page"
          xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
          xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
          xmlns:fn="http://java.sun.com/jsp/jstl/functions"
          xmlns:c="http://java.sun.com/jsp/jstl/core">

    <jsp:directive.attribute name="headerMessage" type="java.lang.String" required="false"/>
    <jsp:directive.attribute name="searchPlaceholderKey" type="java.lang.String" required="true"/>
    <jsp:directive.attribute name="formAction" type="java.lang.String" required="false"/>
    <jsp:directive.attribute name="formInputValue" type="java.lang.String" required="false"/>
    <jsp:directive.attribute name="alwaysShow" type="java.lang.Boolean" required="false"/>
    <jsp:directive.attribute name="enableReset" type="java.lang.Boolean" required="false"/>
    <jsp:directive.attribute name="toggleFiltersOption" type="java.lang.Boolean" required="false"/>

    <c:set var="entrytypes" value="${properties['extendedSearch.entrytypes']}"/>
    <c:set var="searchfields" value="${properties['extendedSearch.searchFields']}"/>

    <c:if test="${not alwaysShow}">
        <c:set var="hideExtSearch" value="display: none;"/>
    </c:if>

    <fmt:message var="searchPlaceholder" key="${searchPlaceholderKey}"/>
    <fmt:message var="fieldPlaceholder" key="search.extended.field.placeholder"/>
    <fmt:message var="yearPlaceholder" key="search.extended.year.placeholder"/>
    <fmt:message var="yearLabel" key="search.extended.year"/>

    <div id="extendedSearch" class="jumbotron search " style="${hideExtSearch}">
        <div class="row">
            <div class="col-md-10">
                <!-- keep me -->
                <c:if test="${not empty headerMessage}">
                    <h2>${headerMessage}</h2>
                </c:if>
            </div>
            <div class="col-md-2" style="padding-top: 20px;">
                <c:if test="${not alwaysShow}">
                    <button class="btn btn-default pull-right" onclick="toggleExtendedSearch('#inpf')">
                        <fontawesome:icon icon="angle-double-up"/>
                    </button>
                </c:if>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-12">
                <form class="form-search" id="extendedSearchForm" action="${formAction}" method="get">
                    <div class="input-group">
                        <span class="input-group-addon"><fontawesome:icon icon="search" size="2x"/></span>
                        <input class="form-control form-control-borderless" type="search"
                               id="extendedSearchInput" name="search" placeholder="${searchPlaceholder}"
                               value="${fn:escapeXml(formInputValue)}"/>
                        <div class="input-group-btn">
                            <c:if test="${enableReset}">
                                <div class="btn bg-white" onclick="resetExtendedSearch()">
                                    <fontawesome:icon icon="times" />
                                </div>
                            </c:if>
                            <button class="btn btn-primary" style="margin-left: 1px;"><fmt:message key="navi.search" /></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <c:if test="${toggleFiltersOption}">
            <div id="extendedFiltersToggle" class="row" style="text-align: right;">
                <div class="col-md-12">
                    <a href="#" id="expandFilterLink" onclick="toggleFilters()"><fmt:message key="search.extended.header" /> <fontawesome:icon icon="angle-double-down" fixedWidth="${true}"/></a>
                    <a href="#" id="hideFilterLink" style="display: none;" onclick="toggleFilters()"><fmt:message key="search.extended.hide" /> <fontawesome:icon icon="angle-double-up" fixedWidth="${true}"/></a>
                </div>
            </div>
            <c:set var="displayFilters" value="display: none;"/>
        </c:if>

        <!-- filters -->
        <div id="extendedFilters" style="${displayFilters}">
            <c:if test="${toggleFiltersOption}">
                <div class="row" style="padding-top: 20px;">
                    <div class="col-md-12">
                        <hr/>
                    </div>
                </div>
            </c:if>
            <div class="form-group row">
                <!-- search attribute field -->
                <div class="col-md-8">
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span id="filterSelectionField"><span class="unselected"><!-- keep me --></span>
                                    <fmt:message key="search.extended.field"/>
                                </span> <span class="caret"><!-- keep me  --></span>
                            </button>
                            <ul id="dropdownSelectionField" class="dropdown-menu">
                                <c:forEach var="field" items="${fn:split(searchfields, ',')}">
                                    <li data-field="${field}">
                                        <a onclick="return switchSelection('Field', '${field}', this)" class="dropdown-item">
                                            <fmt:message key="search.extended.field.${field}"/>
                                        </a>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                        <input class="form-control" type="text" id="filterValueField" placeholder="${fieldPlaceholder}"/>
                    </div>
                </div>

                <!-- entrytypes -->
                <div class="col-md-4">
                    <div class="btn-group btn-block">
                        <button class="btn btn-default btn-block dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span id="filterSelectionEntrytype"><span class="unselected"><!-- keep me --></span>
                            <fmt:message key="search.extended.entrytype"/>
                        </span> <span class="caret"><!-- keepme --></span>
                        </button>
                        <ul id="dropdownSelectionEntrytype" class="dropdown-menu">
                            <c:forEach var="entrytype" items="${fn:split(entrytypes, ',')}" varStatus="status">
                                <fmt:message key="post.resource.entrytype.${entrytype}.title" var="entrytypeMessage" />
                                <c:if test="${fn:startsWith(entrytypeMessage, '???')}">
                                    <c:set var="entrytypeMessage" value="${entrytype}" />
                                </c:if>
                                <li data-entrytype="${entrytype}" data-entrytype-title="${entrytypeMessage}">
                                    <a onclick="return switchSelection('Entrytype', '${entrytype}', this)" class="dropdown-item">
                                        <c:out value="${entrytypeMessage}"/>
                                    </a>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <!-- year -->
                <div id="inputGroupYear" class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <fmt:message key="search.extended.year"/>
                        </div>
                        <input type="text" class="form-control" id="filterValueYear" placeholder="${yearPlaceholder}" aria-label="${yearLabel}"/>
                    </div>
                </div>

                <div id="inputGroupYearRange" class="col-md-4 hidden">
                    <div class="input-group">
                        <div class="input-group-addon"><fmt:message key="search.extended.year.range"/></div>
                        <fmt:message var="fromLabel" key="search.extended.from"/>
                        <input type="text" class="form-control" id="filterValueFromYear" placeholder="${fromLabel}" aria-label="${fromLabel}" maxlength="4"/>
                        <div class="input-group-addon" style="padding:6px;">–</div>
                        <fmt:message var="toLabel" key="search.extended.to"/>
                        <input type="text" class="form-control" id="filterValueToYear" placeholder="${toLabel}" aria-label="${toLabel}" maxlength="4"/>
                    </div>
                </div>

                <div class="col-md-3" style="padding:8px 0;">
                    <a id="toggleYearRange" href="#" onclick="toggleYearRange()"><fmt:message key="search.extended.year.range.placeholder"/></a>
                </div>

                <!-- and/or operator for search syntax -->
                <div class="col-md-3">
                    <div class="btn-group btn-block btn-group-toggle" data-toggle="buttons" id="filterOperator" style="display: flex;">
                        <label class="btn btn-default active" style="flex: 1;">
                            <input type="radio" value="AND" autocomplete="off" checked="checked"/> <fmt:message key="search.extended.and"/>
                        </label>
                        <label class="btn btn-default" style="flex: 1;">
                            <input type="radio" value="OR" autocomplete="off"/> <fmt:message key="search.extended.or"/>
                        </label>
                        <!--
                        <label class="btn btn-primary" style="flex: 1;">
                            <input type="radio" value="NOT" autocomplete="off" /> <fmt:message key="search.extended.not"/>
                        </label>
                        -->
                    </div>
                </div>

                <!-- add filter button -->
                <div class="col-md-2">
                    <button type="button" class="btn btn-primary btn-block" onclick="addFilter()">
                        <fmt:message key="search.extended.add"/>
                    </button>
                </div>
            </div>
        </div>
    </div>
</jsp:root>