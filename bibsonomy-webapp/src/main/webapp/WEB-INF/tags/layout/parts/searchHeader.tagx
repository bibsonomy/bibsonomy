<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
		  xmlns:jsp="http://java.sun.com/JSP/Page"
		  xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
		  xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
		  xmlns:fn="http://java.sun.com/jsp/jstl/functions"
		  xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
		  xmlns:c="http://java.sun.com/jsp/jstl/core"
		  xmlns:spring="http://www.springframework.org/tags">

	<jsp:directive.attribute name="headerMessageKey" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="searchPlaceholderKey" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="requPathAndQuery" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="command" type="org.bibsonomy.webapp.command.EntitySearchAndFilterCommand" required="true" />
	<jsp:directive.attribute name="hidePrefixMenu" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="hideMiscPrefixes" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="startOverviewPrefix" type="java.lang.Boolean" required="false" />

	<fmt:message var="searchPlaceholder" key="${searchPlaceholderKey}" />

	<div class="jumbotron search">
		<h2><fmt:message key="${headerMessageKey}" /></h2>
		<form class="form-search" id="group-search-box" action="${requPathAndQuery}">
			<div class="input-group">
				<span class="input-group-addon">
					<fontawesome:icon icon="search" size="2x" />
				</span>
				<input class="form-control form-control-lg form-control-borderless" id="group-search-box-input"
					   type="search" name="search" placeholder="${searchPlaceholder}" value="${fn:escapeXml(command.search)}" />
				<div class="input-group-btn">
					<!-- TODO: do not use inline javascript -->
					<div class="btn bg-white" onclick="$('#group-search-box-input').val('');$('#group-search-box').submit();">
						<fontawesome:icon icon="times" />
					</div>
					<button class="btn btn-primary"><fmt:message key="navi.search" /></button>
				</div>
			</div>
		</form>
		<c:if test="${empty hidePrefixMenu or not hidePrefixMenu}">
			<spring:eval expression="T(org.bibsonomy.common.enums.Prefix).values()" var="prefixes"/>
			<div class="row">
				<div class="col-md-12 text-center">
					<ul class="prefix-list">
						<c:set var="selectedPrefix" value="${command.prefix}" />
						<c:if test="${startOverviewPrefix}">
							<c:set var="startSelected" value="${empty selectedPrefix}" />
							<li class="${startSelected ? 'active' : ''}"><a href="${mtl:setParam(requPathAndQuery, 'prefix', '')}"><fmt:message key="prefix.START" /> </a></li>
						</c:if>
						<c:forEach var="prefix" items="${prefixes}">
							<c:if test="${not hideMiscPrefixes or (prefix ne 'NUMBER' and prefix ne 'OTHER')}">
								<c:set var="prefixSelected" value="${selectedPrefix eq prefix}" />
								<fmt:message key="prefix.${prefix}" var="prefixMessage" />
								<c:if test="${fn:startsWith(prefixMessage, '???')}">
									<c:set var="prefixMessage" value="${prefix}" />
								</c:if>
								<li class="${prefixSelected ? 'active' : ''}">
									<a href="${mtl:removeParam(mtl:setParam(requPathAndQuery, 'prefix', prefix), 'persons.start')}">${prefixMessage}</a>
								</li>
							</c:if>
						</c:forEach>
					</ul>
				</div>
			</div>
		</c:if>
	</div>
</jsp:root>