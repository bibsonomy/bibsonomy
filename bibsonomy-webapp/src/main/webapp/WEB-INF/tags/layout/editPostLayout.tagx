<?xml version="1.0" ?>
<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:tags="urn:jsptagdir:/WEB-INF/tags/tags"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:post="urn:jsptagdir:/WEB-INF/tags/post"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form">

	<jsp:directive.attribute name="command" type="org.bibsonomy.webapp.command.actions.EditPostCommand" required="true"/>
	<jsp:directive.attribute name="requPath" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="resourceType" type="java.lang.String" required="true" description="The type of the resource this page handles (e.g., 'Bookmark', or 'Publication'). First letter upper case, remaining lower case."/>
	<jsp:directive.attribute name="activeTab" type="java.lang.String" required="false"/>
	
	<jsp:directive.attribute name="generalInformation" fragment="true" required="true" description="The form fields to enter general information."/>	
	<jsp:directive.attribute name="detailedInformation" fragment="true" required="false" description="The form fields to enter detailed information."/>
	<jsp:directive.attribute name="extraInformation" fragment="true" required="false" description="Extra information which shall be displayed outside the form."/>
	<jsp:directive.attribute name="postApproval" fragment="true" required="false" description="A checkbox inorder to approve goldstandard posts"/>
	<jsp:directive.attribute name="postOwner" type="org.bibsonomy.model.User" fragment="false" required="true" description="The owner of the currently edited post" />
	
	<layout:editGoldStandardPostLayout generalInformation="${generalInformation}" requPath="${requPath}" 
		resourceType="${resourceType}" 
		command="${command}" 
		detailedInformation="${detailedInformation}" 
		extraInformation="${extraInformation}"
		postApproval="${postApproval}"
		postOwner="${postOwner}"
		>
		
		<jsp:attribute name="headerExtResource">
			<script type="text/javascript" src="${resdir}/javascript/edit${resourceType}.js"><!-- keep me --></script>
		</jsp:attribute>
		
		<jsp:attribute name="tagsAndGroups">
			<!-- tags -->
			<post:tagfield groupPost="${postOwner.name eq command.context.loginUser.name}" containsComma="${command.containsComma}" labelColSpan="3" inputColSpan="9" tags="${command.tags}" recommendedTags="${command.recommendedTags}"/>

			<!-- post visibility -->
			<a name="groups"><!--  --></a>
			<bsform:fieldset legendKey="post.resource.groups.describe">
				<jsp:attribute name="content">
					<post:groupBox groups="${postOwner.groups}" labelColSpan="3"/>
				</jsp:attribute>
			</bsform:fieldset>

			<!-- group options -->
			<c:set var="isGroupPost" value="${not empty postOwner and postOwner ne command.context.loginUser}" />
			<c:if test="${not empty postOwner.groups and not isGroupPost}">
				<c:set var="groups" value="${mtl:filterGroups(postOwner.groups, false)}" />
				<a name="group-options"><!-- keep me --></a>
				<bsform:fieldset legendKey="post.resource.groups.options">
					<jsp:attribute name="content">
						<!-- sent to -->
						<post:sendToGroupBox userGroups="${groups}"
											 selectedGroups="${command.sendToGroups}"
											 selectedPresetTags="${command.selectedPresetTagsByGroup}"
											 labelColSpan="3" inputColSpan="9"/>

						<!-- relevant for -->
						<c:if test="${properties['group.relevantFor.enabled']}">
							<post:relevantForGroupBox groups="${groups}" labelColSpan="3" inputColSpan="9"/>
						</c:if>
						<!-- tag sets -->
						<post:tagSets groups="${groups}" />
					</jsp:attribute>
				</bsform:fieldset>
			</c:if>
		</jsp:attribute>
		<jsp:attribute name="groupsandfriends">
			<!-- groupsandfriends -->
			<a name="groups"><!--  --></a>
			<bsform:fieldset legendKey="post.resource.groupsandfriends">
				<jsp:attribute name="content">
					<post:groupBoxFG groups="${command.context.loginUser.groups}" friends="${command.friendsOfUser}" labelColSpan="3"/>
				</jsp:attribute>
			</bsform:fieldset>
		</jsp:attribute>

	</layout:editGoldStandardPostLayout>
</jsp:root>