<jsp:root version="2.0" xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions">
	
	<jsp:directive.attribute name="showBookmarkletAlternative" type="java.lang.Boolean" required="false" />
	<c:if test="${empty showBookmarkletAlternative}">
		<c:set var="showBookmarkletAlternative" value="${true}" />
	</c:if>
	
	<div id="browser-addon" class="featurette" data-show-alternative="${showBookmarkletAlternative}">
		<div class="row">
			<!-- Heading -->
			<div class="col-md-7">
				<h2 class="featurette-heading">
					<fmt:message key="bookmarklet.addon.headline">
						<fmt:param value="${properties['project.name']}" />
					</fmt:message>
				</h2>
				<!-- Text -->
				<div class="lead">
					<p>
						<fmt:message key="bookmarklet.addon.text1.short">
							<fmt:param value="${properties['project.name']}" />
						</fmt:message>
					</p>
					
					<ul class="checklist">
						<li><span class="fa fa-check" style="color: #0b0;"><!-- keep me --></span>&amp;nbsp
							<fmt:message key="bookmarklet.addon.text1.check1">
								<fmt:param value="${properties['project.name']}" />
							</fmt:message>
						</li>
						<li>
							<span class="fa fa-check" style="color: #0b0;"><!-- keep me --></span>&amp;nbsp
							<fmt:message key="bookmarklet.addon.text1.check2" />
						</li>
						<li>
							<span class="fa fa-check" style="color: #0b0;"><!-- keep me --></span>&amp;nbsp
							<fmt:message key="bookmarklet.addon.text1.check3" />
						</li>
					</ul>
					<!-- Firefox button -->
					<p class="firefox-addon">
						<a href="${properties['project.addons.firefox.url']}" class="btn btn-success btn-lg btn-block">
							<fmt:message key="bookmarklet.buttonTextFirefox" />
						</a>
					</p>
					
					<!-- Chrome button -->
					<p class="chrome-addon">
						<a href="${properties['project.addons.chrome.url']}" class="btn btn-success btn-lg btn-block">
							<fmt:message key="bookmarklet.buttonTextChrome" />
						</a>
					</p>
					
					<!-- Safari buttons -->
					<p class="safari-addon">
						<a class="btn btn-success btn-lg btn-block" href="${properties['project.addons.safari.url']}">
							<fmt:message key="bookmarklet.buttonTextSafari" />
						</a>
					</p>
				</div>
			</div>
			<div class="col-md-5">
				<!-- FIREFOX -->
				<div class="firefox-addon">
					<!-- Screenshot -->
					<span class="img-thumbnail img-responsive">
						<img class="featurette-image img-responsive" src="/resources/image/addons/${locale}/bookmarklet_screenshot_firefox.png" />
						<br />
						<fmt:message key="bookmarklet.addon.text2">
							<fmt:param value="${properties['project.name']}" />
						</fmt:message>
						
					</span>
				</div>
				
				<!-- Chrome -->
				<div class="chrome-addon">
					<span class="img-thumbnail img-responsive">
						<img class="featurette-image img-responsive" src="/resources/image/addons/${locale}/bookmarklet_screenshot_chrome.png" />
						<br />
						<fmt:message key="bookmarklet.addon.text3">
							<fmt:param value="${properties['project.name']}" />
						</fmt:message>
					</span>
				</div>
				
				<!-- Safari -->
				<div class="safari-addon">
					<span class="img-thumbnail img-responsive">
						<img class="featurette-image img-responsive" src="/resources/image/addons/${locale}/bookmarklet_screenshot_safari.png" />
						<br />
						<fmt:message key="bookmarklet.addon.text2">
							<fmt:param value="${properties['project.name']}" />
						</fmt:message>
					</span>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${showBookmarkletAlternative}">
		<hr class="featurette-divider" />
	</c:if>
	<div class="featurette" id="bookmarklets">
		<!-- ======================================================== -->
		<!-- SECOND PART: (Bookmarklet buttons on your bookmarks bar) -->
		<!-- ======================================================== -->
		
		<!-- Heading -->
		<h2 class="featurette-heading">
			<fmt:message key="buttons.bookmarklet1" />
		</h2>
		
		<!-- Subheading -->
		<div class="lead" id="allBrowsers">
			<p class="addonhint">
				<fmt:message key="buttons.mybibsonomy_intro" />
			</p>
			<p class="bookmarklethint">
				<fmt:message key="buttons.bookmarklet.intro" />
			</p>
		</div>
		<div id="internetExplorer">
			<p>
				<fmt:message key="buttons.mybibsonomy_internetExplorer" />
			</p>
		</div>
		<!-- my bibsonomy -->
		<div class="row featurette">
			<div class="col-xs-5 col-lg-4 text-right">
				<buttons:myBibSonomy />&amp;nbsp
			</div>
			<div class="col-xs-7 col-lg-8">
				<p class="lead">
					<span class="fa fa-arrow-left" style="color: #0b0;"><!-- keep me --></span>&amp;nbsp
					<fmt:message key="buttons.mybibsonomy_mybibsonomy" />
				</p>
			</div>
		</div>
		<!-- bookmark in this window -->
		<div class="row featurette">
			<div class="col-xs-5 col-lg-4 text-right">
				<p class="lead">
					<buttons:postBookmark />&amp;nbsp
				</p>
			</div>
			<div class="col-xs-7 col-lg-8">
				<p class="lead">
					<span class="fa fa-arrow-left" style="color: #0b0;"><!-- keep me --></span>&amp;nbsp
					<fmt:message key="buttons.mybibsonomy_bookmark_here" />
				</p>
			</div>
		</div>
		
		<!-- bookmark in a new window -->
		<div class="row featurette">
			<div class="col-xs-5 col-lg-4 text-right">
				<buttons:postBookmarkPopUp />&amp;nbsp
			</div>
			<div class="col-xs-7 col-lg-8">
				<p class="lead">
					<span class="fa fa-arrow-left" style="color: #0b0;"><!-- keep me --></span>&amp;nbsp
					<fmt:message key="buttons.mybibsonomy_bookmark_neww" />
				</p>
			</div>
		</div>
		<!-- publication in this window -->
		<div class="row featurette">
			<div class="col-xs-5 col-lg-4 text-right">
				<buttons:postPublication />&amp;nbsp
			</div>
			<div class="col-xs-7 col-lg-8">
				<p class="lead">
					<span class="fa fa-arrow-left" style="color: #0b0;"><!-- keep me --></span>&amp;nbsp
				<fmt:message key="buttons.mybibsonomy_publication_here" />
				</p>
			</div>
		</div>
		
		<!-- publication in a new window -->
		<div class="row featurette">
			<div class="col-xs-5 col-lg-4 text-right">
				<buttons:postPublicationPopUp />&amp;nbsp
			</div>
			<div class="col-xs-7 col-lg-8">
				<p class="lead">
					<span class="fa fa-arrow-left" style="color: #0b0;"><!-- keep me --></span>&amp;nbsp
					<fmt:message key="buttons.mybibsonomy_publication_neww" />
				</p>
			</div>
		</div>
	</div>
	
	<!-- Browser detection -->
	<script type="text/javascript">
	$(function() {
		var availableAddons = "${properties['project.addons.available']}";
		var ua = navigator.userAgent.toLowerCase();

		console.log("Browser: " + ua);
		$("#browser-addon").hide();
		
		$(".firefox-addon").hide();
		$(".chrome-addon").hide();
		$(".safari-addon").hide();
		
		$(".addonhint").hide();
		$(".bookmarklethint").hide();
		$("#internetExplorer").hide();
		
		if (!availableAddons.length) {
			$(".bookmarklethint").show();
			return;
		}
		
		if (ua.indexOf("firefox") > -1) { //this is firefox
			showAddOn("firefox");
		} else if (ua.indexOf("opr") > -1) {
			// this is opera
		} else if (ua.indexOf("chrome") > -1) { //this is chrome
			showAddOn("chrome");
		} else if (ua.indexOf("safari") > -1) { //this is safari
			showAddOn("safari");
		} else if (ua.indexOf("MSIE") > -1) { // this is IE
			$("#internetExplorer").show();
		}
	});
	
	function showAddOn(addon) {
		var addonContainer = $("#browser-addon");
		addonContainer.show();
		$("." + addon + "-addon").show();
		$(".addonhint").show();
		var showAlternative = addonContainer.data('show-alternative');
		if (!showAlternative) {
			$('#bookmarklets').hide();
		}
	}
	</script>
</jsp:root>