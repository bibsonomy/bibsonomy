<jsp:root version="2.0" 
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:bib="urn:jsptagdir:/WEB-INF/tags/resources/bibtex"
	xmlns:pub="urn:jsptagdir:/WEB-INF/tags/resources/publication"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:export="urn:jsptagdir:/WEB-INF/tags/export/bibtex"
	xmlns:resource="urn:jsptagdir:/WEB-INF/tags/resources"
	xmlns:tags="urn:jsptagdir:/WEB-INF/tags/tags"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:editpub="urn:jsptagdir:/WEB-INF/tags/actions/edit/publication"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
	xmlns:discussion="urn:jsptagdir:/WEB-INF/tags/resources/discussion"
	xmlns:bs="urn:jsptagdir:/WEB-INF/tags/layout/bootstrap"
	xmlns:bsold="urn:jsptagdir:/WEB-INF/tags/bs"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:fontawsome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
	xmlns:user="urn:jsptagdir:/WEB-INF/tags/resources/user"
	xmlns:common="urn:jsptagdir:/WEB-INF/tags/resources/common"
	xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
>
	
	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true" description="The publication post that should be rendered."/>
	<jsp:directive.attribute name="referer" type="java.lang.String" required="true" description="The referer to the scraped site"/>
	
	<c:set var="publication" value="${post.resource}"/>
	<c:set var="isOwnEntry" value="${post.user.name eq command.context.loginUser.name}"/>
		
	<!--+
		| abstract
		+-->
	<c:if test="${not empty post.resource['abstract']}">
		<h4 class="pub-details"><fontawesome:icon icon="align-justify" fixedWidth="${true}" spaceAfter="5"/><fmt:message key="publications.abstract.headline"/></h4>
		<div id="abstract" class="show-more">
			<span class="contentContainer"><c:out value="${mtl:cleanBibtex(post.resource['abstract'])}" /></span>
		</div>
	</c:if>

	<!--+
		| description
		+-->
	<c:if test="${not empty post.description}">
		<h4 class="pub-details"><fontawesome:icon icon="ellipsis-h" fixedWidth="${true}" spaceAfter="5"/><fmt:message key="post.resource.description.cap"/></h4>
		<div id="desc" class="show-more">
			<span itemprop="description"><c:out value="${post.description}" /></span>
		</div>
	</c:if>

	<!--+
		|
		| resources (URLs/PDF/etc.)
		|
		+-->
	<h4 class="pub-details"><fontawesome:icon icon="link" fixedWidth="${true}" spaceAfter="5"/><fmt:message key="publications.resource.headline" /></h4>

	<!--+
		| full description table
		+-->
	<div class="goldstandard-metadata">
		<bib:allFieldsTable publication="${publication}" post="${post}" isOwnEntry="${isOwnEntry}" />
	</div>

	<!--+
		| tag list
		+-->
	<h4 class="pub-details"><fontawesome:icon icon="tag" fixedWidth="${true}" spaceAfter="5"/><fmt:message key="publications.tags.headline"/></h4>
	<div class="tag-list">
		<ul class="list-inline">
		<c:forEach var="tag" items="${post.visibleTags}" varStatus="status">
			<li>
			<c:set var="lblColor" value="primary" />
			<!-- TODO: remove special handling for myown -->
			<c:if test="${tag.name eq 'myown'}"> <c:set var="lblColor" value="warning" /> </c:if>
				<span class="label label-${lblColor}">
					<a rel="nofollow" href="${relativeUrlGenerator.getUserUrlByUserNameAndTagName(post.user.name, tag.name)}" title=""> <c:out value="${tag.name}"/> </a>
				</span>
			</li>
		</c:forEach>
		<c:if test="${isOwnEntry}">
			<c:forEach var="tag" items="${post.hiddenSystemTags}" varStatus="status">
				<li> 
					<span class="label label-warning">
						<a href="${relativeUrlGenerator.getUserUrlByUserNameAndTagName(post.user.name, tag.name)}" title="">
							<c:out value="${tag.name}"/>
						</a>
					</span>
				</li>
			</c:forEach>
		</c:if>
		</ul>
	</div>

	<!--+
        | Community section - only shown if there are related users
        +-->
	<c:if test="${fn:length(command.relatedUserCommand.relatedUsers) gt 1}">
		<h4 class="pub-details"><fontawsome:icon icon="users" fixedWidth="${true}" spaceAfter="5"/><fmt:message key="community" /></h4>

		<ul class="nav nav-tabs publicationpage-tabs" role="tablist">
			<li class="active"><a data-toggle="tab" href="#communityUsers"><fmt:message key="bibtex.details.related_users"/></a></li>
			<li><a data-toggle="tab" href="#communityTags"><fmt:message key="tags"/></a></li>
		</ul>
		<div class="tab-content publicationpage-tab-content">
			<div id="communityUsers" class="tab-pane active">
				<common:relatedUsers
						userList="${command.relatedUserCommand.relatedUsers}"
						interHash="${post.resource.interHash}"
						showHeadline="false"
				/>
			</div>

			<div id="communityTags" class="tab-pane">
				<small style="font-size:80%;">
					<user:username user="${post.user}" />
					<fmt:message key="tags.highlighted.by_user"/>
				</small>
				<c:set var="tagsString" value="${mtl:toTagString(post.tags)}" />

				<tags:cloud requPath="tag" cmd="${command.tagcloud}" tagSizeMode="detail" tagboxMinfreqStyle="none" highlightTags="${tagsString}"/>
			</div>
		</div>
	</c:if>


</jsp:root>
