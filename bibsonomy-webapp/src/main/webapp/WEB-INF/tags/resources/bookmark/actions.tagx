<!-- TODO: abstract view for actions @see bibtex/actions -->
<jsp:root version="2.0" 
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form">

	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true" />
	<jsp:directive.attribute name="loginUserName" type="java.lang.String" required="true" />
	
	<c:set var="isRemotePost" value="${not empty post.systemUrl and not mtl:isSameHost(post.systemUrl, properties['project.home'])}" />

	<c:set var="rscType" value="bookmark" />

	<div class="edit-media-buttons btn-group btn-group-xs dropdown-align-right">
		<c:set var="buttonClasses" value="btn btn-default btn-xsmall"/>

		<!--+
			| COPY/EDIT
			+-->
		<c:choose>
			<c:when test="${command.context.userLoggedIn}">
				<c:choose>
					<!-- COPY -->
					<c:when test="${isRemotePost}">
						<c:url var="copyLink" value="/ShowBookmarkEntry">
							<c:param name="url" value="${mtl:cleanUrl(post.resource.url)}"/>
							<c:param name="description" value="${post.description}"/>
							<c:param name="extended"/>
							<c:param name="referer" value="${mtl:cleanUrl(post.resource.url)}"/>
						</c:url>
						<fmt:message key="bookmark.actions.copy.title" var="copyTitle"/>
						<a class="${buttonClasses}" href="${copyLink}" title="${fn:escapeXml(copyTitle)}">
							<span class="fa fa-copy"><!--  --></span>
						</a>
					</c:when>
					<c:otherwise>
						<c:choose>
							<!-- COPY -->
							<c:when test="${loginUserName ne post.user.name}"><!-- user is not post's owner -->
								<c:url var="copyLink" value="/editBookmark">
									<c:param name="hash" value="${post.resource.intraHash}" />
									<c:param name="user" value="${post.user.name}" />
									<c:param name="copytag" value="${mtl:toTagString(post.tags)}" />
								</c:url>
								<fmt:message key="bookmark.actions.copy.title" var="copyTitle"/>
								<a class="${buttonClasses}" href="${copyLink}" title="${fn:escapeXml(copyTitle)}">
									<span class="fa fa-copy"><!--  --></span>
								</a>
							</c:when>

							<!-- EDIT -->
							<c:otherwise><!-- user is post's owner -->
								<fmt:message key="bookmark.actions.edit.title" var="editTitle" />
								<a class="${buttonClasses}" href="/editBookmark?intraHashToUpdate=${post.resource.intraHash}" title="${editTitle}">
									<span class="fa fa-pencil"><!--  --></span>
								</a>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<!-- For signed out users we always show the inactive copy button -->
				<fmt:message key="bookmark.actions.copy.inactive" var="copyTitle" />
 				<a class="${buttonClasses}" title="${copyTitle}" disabled="disabled">
 					<span class="fa fa-copy"><!--  --></span>
 				</a>
			</c:otherwise>
		</c:choose>
	
		<!--+
			| link for the memento timegate to the archived version of the page
			+-->
		<fmt:message key="bookmark.actions.memento" var="mementoTitle" />
		<a class="${buttonClasses}" href="${properties['memento.timegate']}/${mtl:formatDateMemento(post.date)}/${fn:escapeXml(post.resource.url)}" rel="nofollow" title="${mementoTitle}">
		   <span class="fa fa-history"><!--  --></span>
		</a>
	

		<!--+
			| REMOVE/DELETE
			+ -->
		<c:choose>
			<c:when test="${post.inboxPost}">
				<!-- INBOX REMOVE -->
				<!-- Inbox can be viewed only by signed in users -->
				<c:url var="message_remove" value="/removeMessage">
					<c:param name="hash" value="${post.resource.intraHash}" />
					<c:param name="user" value="${post.user.name}" />
					<c:param name="ckey" value="${ckey}" />
					<c:param name="clear" value="false" />
				</c:url>
				<fmt:message key="bookmark.actions.removeMessage.title" var="removeTitle" />
				<a class="${buttonClasses}" href="${message_remove}" title="${removeTitle}" data-type="inboxpost">
					<span class="fa fa-times"><!--  --></span>
				</a>
			</c:when>
			<c:otherwise>
				<!-- REGULAR POST DELETE -->
				<c:choose>
					<c:when test="${command.context.userLoggedIn and loginUserName eq post.user.name and not isRemotePost}">
						<fmt:message key="bookmark.actions.delete.title" var="deleteTitle" />
						<c:url var="deleteUrl" value="/deletePost">
							<c:param name="resourceHash" value="${post.resource.intraHash}" />
							<c:param name="ckey" value="${ckey}" />
						</c:url>
						<a class="${buttonClasses} confirmdelete" href="${deleteUrl}" title="${deleteTitle}" data-type="post">
							<span class="fa fa-times"><!--  --></span>
						</a>
					</c:when>
					<c:otherwise>
						<fmt:message key="bookmark.actions.delete.inactive" var="deleteTitle"/>
						<span class="${buttonClasses}" title="${deleteTitle}" disabled="disabled">
							<span class="fa fa-times"><!--  --></span>
						</span>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>

	
	
		<button class="btn btn-default btn-small dropdown-toggle" data-toggle="dropdown">
			<span class="caret"><!-- KEEP ME --></span>
		</button>

		<ul class="dropdown-menu" style="min-width: 230px;">
			<!--+
				| COMMUNITY POST
				+ -->
			<li>
				<fmt:message key="actions.communityPost.title" var="communityPost" />
				<a href="${relativeUrlGenerator.getCommunityBookmarkUrlByInterHash(post.resource.interHash)}" title="${fn:escapeXml(communityPost)}">
					<fmt:message key="actions.communityPost" />
				</a>
			</li>

			<!--+
				| HISTORY
				+ -->
			<li>
				<fmt:message key="bookmark.actions.posthistory.title" var="postHistoryTitle" />
				<c:choose>
					<c:when test="${(command.context.userLoggedIn and (loginUserName eq post.user.name)) or (empty post.user)}">
						<a href="${relativeUrlGenerator.getHistoryURLByHashAndUserName(post.resource.interHash, post.user.name, true)}" title="${postHistoryTitle}">
							<fmt:message key="bookmark.actions.posthistory.title" />
						</a>
					</c:when>
					<c:otherwise>
						<span class="ilitem" title="${historyTitle}">
							<fmt:message key="bookmark.actions.posthistory.title" />
						</span>
					</c:otherwise>
				</c:choose>
			</li>

		</ul>
	</div>
</jsp:root>