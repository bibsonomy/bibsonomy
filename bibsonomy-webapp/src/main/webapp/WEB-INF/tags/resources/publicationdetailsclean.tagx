<jsp:root version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:pub="urn:jsptagdir:/WEB-INF/tags/resources/publication"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:editpub="urn:jsptagdir:/WEB-INF/tags/actions/edit/publication"
	xmlns:bs="urn:jsptagdir:/WEB-INF/tags/bs"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/resources/actionbuttons"
	xmlns:export="urn:jsptagdir:/WEB-INF/tags/export/bibtex">
	
	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true" description="The publication post that should be rendered."/>
	<jsp:directive.attribute name="referer" type="java.lang.String" required="true" description="The referer to the scraped site"/>
	
	<c:set var="publication" value="${post.resource}"/>
	<c:set var="isOwnEntry" value="${post.user.name eq command.context.loginUser.name}"/>
	<c:set var="isEditableByUser" value="${false}" />
	<c:forEach var="group" items="${command.context.loginUser.groups}">
		<c:if test="${group.name eq post.user.name and group.getGroupMembershipForUser(command.context.loginUser.name).groupRole eq 'ADMINISTRATOR'}">
			<c:set var="isEditableByUser" value="${true}" />
		</c:if>
	</c:forEach>
	
	<!-- modal for exports -->
	<export:modal />
	
	<c:choose>
		<c:when test="${post.resource.entrytype eq 'book'}">
			<c:set var="itemtype" value="http://schema.org/Book" />
		</c:when>
		<c:otherwise>
			<c:set var="itemtype" value="http://schema.org/ScholarlyArticle" />
		</c:otherwise>
	</c:choose>
	
	<div itemscope="itemscope" itemtype="${itemtype}">
		<div class="title">
			<pub:title post="${post}">
				<jsp:attribute name="pubActionButtons">
					<div class="pull-right">
						<buttons:actions post="${post}" loginUserName="${command.context.loginUser.name}" disableResourceLinks="${false}" resourceType="bibtex"/>
					</div>
				</jsp:attribute>
			</pub:title>
		</div>

		<hr />

		<c:choose>
			<!-- if own entry, render a two column layout to display documents on the right side -->
			<c:when test="${isOwnEntry or isEditableByUser or not empty publication.documents}">
				<bs:twoColumns columnSizeLeft="8" columnSizeRight="4">
					<jsp:attribute name="columnLeft">
						<!-- PUBLICATION DETAILS -->
						<pub:publicationdetailscontent post="${post}" referer="${referer}" />
					</jsp:attribute>

					<jsp:attribute name="columnRight">
						<div class="documents">
							<!-- META DATA ABOUT ATTACHED DOCUMENTS  -->
							<pub:doc post="${post}" />
						</div>
					</jsp:attribute>
				</bs:twoColumns>
			</c:when>

			<!-- if not own entry, render a one column layout to display content details -->
			<c:otherwise>
				<div class="row">
					<!-- PUBLICATION DETAILS -->
					<div class="col-md-12">
						<pub:publicationdetailscontent post="${post}" referer="${referer}" />
					</div>
				</div>
			</c:otherwise>
		</c:choose>

		<!-- only shown on PUMA (transfer publication to OpenAccess repository) -->
		<c:if test="${properties['publication.reporting.enabled'] eq 'true'}">
			<div class="row">
				<div class="col-md-12">
					<c:set var="reportingMode" value="${properties['publication.reporting.mode']}" />
					<!-- show only, if publication is own one (tagged myown) -->
					<c:if test="${isOwnEntry and mtl:hasTagMyown(post)}">
						<hr />
						<editpub:openaccess post="${post}" bibtex="${publication}" reportingMode="${reportingMode}" referer="${referer}"/>
					</c:if>
				</div>
			</div>
		</c:if>
	</div>
</jsp:root>