<jsp:root version="2.0"
		  xmlns="http://www.w3.org/1999/xhtml"
		  xmlns:jsp="http://java.sun.com/JSP/Page"
		  xmlns:c="http://java.sun.com/jsp/jstl/core"
		  xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
		  xmlns:form="http://www.springframework.org/tags/form"
		  xmlns:bib="urn:jsptagdir:/WEB-INF/tags/resources/bibtex"
		  xmlns:export="urn:jsptagdir:/WEB-INF/tags/export/bibtex"
		  xmlns:tags="urn:jsptagdir:/WEB-INF/tags/tags"
		  xmlns:pub="urn:jsptagdir:/WEB-INF/tags/resources/bibtex"
		  xmlns:pub2="urn:jsptagdir:/WEB-INF/tags/resources/publication"
		  xmlns:gpub="urn:jsptagdir:/WEB-INF/tags/resources/goldstandardpublication"
		  xmlns:ref="urn:jsptagdir:/WEB-INF/tags/resources/goldstandardpublication/references"
		  xmlns:discussion="urn:jsptagdir:/WEB-INF/tags/resources/discussion"
		  xmlns:review="urn:jsptagdir:/WEB-INF/tags/resources/discussion/review"
		  xmlns:fn="http://java.sun.com/jsp/jstl/functions"
		  xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
		  xmlns:bs="urn:jsptagdir:/WEB-INF/tags/layout/bootstrap"
		  xmlns:goldpub="urn:jsptagdir:/WEB-INF/tags/resources/publication"
		  xmlns:goldstandardpublication="urn:jsptagdir:/WEB-INF/tags/resources/goldstandardpublication"
		  xmlns:goldstandard="urn:jsptagdir:/WEB-INF/tags/resources/goldstandard"
		  xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
		  xmlns:common="urn:jsptagdir:/WEB-INF/tags/resources/common"
		  xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/resources/actionbuttons"
		  xmlns:bsold="urn:jsptagdir:/WEB-INF/tags/bs"
		  xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
		  xmlns:fontawsome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
		  xmlns:sidebar="urn:jsptagdir:/WEB-INF/tags/layout/sidebar"
>

	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true"/>
	<jsp:directive.attribute name="postOfLoggedInUser" type="org.bibsonomy.model.Post" required="false"/>
	<jsp:directive.attribute name="loginUser" type="org.bibsonomy.model.User" required="true"/>
	<jsp:directive.attribute name="postUserName" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="postIsPublic" type="java.lang.Boolean" required="true"/>

	<!-- TODO: implement -->
	<c:set var="showFolkSonomyLinks" value="${true}"/>
	<!-- in cris mode only show folksonomy links to loggedin users -->
	<c:if test="${properties['system.cris'] eq 'true'}">
		<c:set var="showFolkSonomyLinks" value="${not empty loginUser.name}" />
	</c:if>

	<c:set var="isAdmin" value="${loginUser.role eq 'ADMIN'}" />

	<c:set var="goldstandard" value="${post.resource}" />

	<c:set var="isRealGoldstandard" value="${goldstandard['class'].simpleName eq 'GoldStandardPublication' }" />
	<c:set var="hasRelatedPublication" value="${isRealGoldstandard and (not empty goldstandard.references or not empty goldstandard.referencedBy or not empty goldstandard.subGoldStandards)}" />
	<c:set var="publishedIn" value="" />
	<c:if test="${isRealGoldstandard and not empty goldstandard.referenceThisPublicationIsPublishedIn}">
		<c:set var="publishedIn" value="${goldstandard.referenceThisPublicationIsPublishedIn.iterator().next()}" />
	</c:if>

	<!-- determine the publication type for schema.org markup -->
	<c:set var="itemType" value="http://schema.org/ScholarlyArticle"/>
	<c:if test="${not empty goldstandard.entrytype and ('book' eq goldstandard.entrytype)}">
		<c:set var="itemType" value="http://schema.org/Book"/>
	</c:if>

	<div class="goldstandard-details" >
		<div itemtype="${itemType}" itemscope="itemscope" id="${goldstandard.interHash}">

			<!--+
				| HEADER
				+-->
			<div class="row" id="info" data-interhash="${goldstandard.interHash}">
				<div class="col-md-10">
					<div class="content-header-entrytype">
						<c:if test="${post.approved}">
							<fmt:message key="goldStandard.approved.title" var="approved"/>
							<span class="community-approved" data-toggle="tooltip" data-placement="bottom" title="${fn:escapeXml(approved)}">
								<fontawesome:icon icon="check-circle" />
							</span>
						</c:if>
						<span class="content-header-entrytype-badge">
							<bib:entrytype publication="${goldstandard}" />
						</span>
					</div>

					<h1 itemprop="name">
						<c:out value="${mtl:cleanBibtex(goldstandard.title)}" />
					</h1>
					<bib:desc publication="${goldstandard}"
							  post="${post}"
							  linebreakAfterAuthors="${true}"
							  restrictToFirstAuthors="${false}"
							  onlyLinkAuthorsWithPersons="${not showFolkSonomyLinks}"
							  publishedIn="${publishedIn}"/>
					<c:set var="doi" value="${goldstandard.miscFields['doi']}" />
					<c:if test="${not empty doi}">
						<br />
						<span class="additional-entrytype-information">
						DOI: <a href="https://dx.doi.org/${doi}"><c:out value="${doi}" /></a>
						</span>
					</c:if>
				</div>

				<div class="col-md-2 content-header-metric-buttons hidden-xs">
					<c:if test="${showFolkSonomyLinks}">
						<div class="metric-info">
							<fmt:message key="goldStandard.users" var="inCollectionOf"/>
							<a class="content-header-additional-info-item hidden-xs"
							   data-toggle="tooltip"
							   data-placement="bottom"
							   title="${fn:escapeXml(inCollectionOf)}"
							   href="#goldStandardPublicationUsers"
							>
								<fontawesome:icon icon="users" />
								<span class="badge"><c:out value="${fn:length(command.relatedUserCommand.relatedUsers)}" /></span>
							</a>
						</div>
						<div class="metric-info">
							<fmt:message key="tags.of_resource" var="tagsTotal"/>
							<a class="content-header-additional-info-item hidden-xs"
							   data-toggle="tooltip"
							   data-placement="bottom"
							   title="${fn:escapeXml(tagsTotal)}"
							   href="#goldStandardPublicationTags"
							>
								<fontawesome:icon icon="tags" />
								<span class="badge"><c:out value="${fn:length(command.tagcloud.tags)}" /></span>
							</a>
						</div>
					</c:if>

					<c:if test="${hasRelatedPublication or (isAdmin and isRealGoldstandard)}">
						<c:set var="hasCitations" value="${not empty goldstandard.referencedBy}" />
						<c:set var="hasReferences" value="${not empty goldstandard.references}" />
						<c:if test="${hasReferences or hasCitations}">
							<div class="metric-info">
								<fmt:message key="goldStandard.resource.info.citations" var="referencedByTotal"/>
								<fmt:message key="goldStandard.resource.info.references" var="referencesTotal"/>
								<c:set var="linkTitle" value="${hasReferences ? referencesTotal : ''} ${hasCitations ? referencedByTotal : ''}" />
								<a class="content-header-additional-info-item"
								   data-toggle="tooltip"
								   data-placement="bottom"
								   title="${fn:escapeXml(linkTitle)}"
								   href="#goldStandardPublicationReferences"
								>
									<fontawesome:icon icon="link" />
									<span class="badge">
										<c:if test="${hasReferences}">
											<c:out value="${fn:length(goldstandard.references)}" />
										</c:if>
										<c:if test="${hasReferences and hasCitations}">
											<c:out value=" + " />
										</c:if>
										<c:if test="${hasCitations}">
											<c:out value="${fn:length(goldstandard.referencedBy)}" />
										</c:if>
									</span>
								</a>
							</div>
						</c:if>

					</c:if>
				</div>
			</div>

			<div class="goldstandard-actions row">
				<div class="col-md-12 content-header-buttons">
					<c:set var="isLinkToDocument" value="${mtl:isLinkToDocument(goldstandard.url)}" />
					<c:if test="${isLinkToDocument}">
						<a href="${fn:escapeXml(goldstandard.url)}" class="btn btn-primary">
							<fontawesome:icon icon="file-pdf-o" /><c:out value=" " /><fmt:message key="fulltext"/>
						</a>
					</c:if>

					<c:if test="${command.context.userLoggedIn}">

						<buttons:communityPostActionButtons
								post="${post}"
								resourceType="bibtex"
								communityPostExists="${isRealGoldstandard}"
								postOfLoggedInUser="${postOfLoggedInUser}"
								ckey="${fn:escapeXml(command.context.ckey)}"
						/>
						<c:if test="${not empty postOfLoggedInUser}">
							<span class="community-info warning">
								<fontawsome:icon icon="warning" /><c:out value=" " />
								<fmt:message key="community.warning.view" /><c:out value=" " />

								<fmt:message key="community.warning.edit">
									<fmt:param value="${relativeUrlGenerator.getPostUrl(postOfLoggedInUser)}" />
								</fmt:message>

							</span>
						</c:if>
					</c:if>
				</div>
			</div>

			<hr />

			<!--+
                | CONTENT
                +-->
			<div class="goldstandard-content row">
				<div class="col-md-9 col-sm-12 col-xs-12 content">

					<!--+
						| abstract
						+-->
					<c:choose>
						<c:when test="${not empty goldstandard['abstract']}">
							<h3 class="pub-details">
								<fontawesome:icon icon="align-justify" />
								<c:out value=" "/>
								<fmt:message key="publications.abstract.headline"/>
							</h3>
							<div id="abstract">
								<div class="show-more" itemprop="description">
									<span class="contentContainer"><c:out value="${mtl:cleanBibtex(goldstandard['abstract'])}" /></span>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<h3 class="pub-details">
								<fontawesome:icon icon="align-justify" />
								<c:out value=" "/>
								<fmt:message key="bibtex.details.meta"/>
							</h3>
						</c:otherwise>
					</c:choose>

					<!--+
						| full description table
						+-->
					<div class="goldstandard-metadata">
						<bib:allFieldsTable publication="${goldstandard}" post="${post}" publishedIn="${publishedIn}" />
					</div>

					<!-- tags -->
					<c:if test="${showFolkSonomyLinks}">
						<h3 id="goldStandardPublicationTags">
							<fontawesome:icon icon="tag" />
							<c:out value=" "/>

							<fmt:message key="publications.tags.headline"/>
							<c:set var="tagsString" value="" />
							<c:if test="${not empty postOfLoggedInUser.tags}">
								<small>
									<c:out value=" " /><fmt:message key="community.tags.myown"/>
								</small>

								<c:set var="tagsString" value="${mtl:toTagString(postOfLoggedInUser.tags)}" />
							</c:if>
						</h3>

						<c:set var="tagSizeMode" value="detail" />
						<c:if test="${command.tagcloud.style.id == 0}">
							<c:set var="tagSizeMode" value="user" />
						</c:if>

						<tags:cloud
								requPath="tag"
								cmd="${command.tagcloud}"
								tagSizeMode="${tagSizeMode}"
								showTooltips="${command.context.loginUser.settings.tagboxTooltip}"
								highlightTags="${tagsString}"
						/>

					</c:if>

					<!-- users that have this resource in their collection -->
					<c:if test="${showFolkSonomyLinks}">
						<h3 id="goldStandardPublicationUsers">
							<fontawesome:icon icon="user" />
							<c:out value=" "/>

							<fmt:message key="goldstandard.resource.relatedUsers"/>
						</h3>

						<common:relatedUsers
								userList="${command.relatedUserCommand.relatedUsers}"
								interHash="${goldstandard.interHash}"
								showHeadline="false"
						/>
					</c:if>

					<!-- Referenced and Referencing posts -->
					<c:if test="${hasRelatedPublication or (isAdmin and isRealGoldstandard)}">
						<c:set var="tabActive" value="false" />
						<div id="related-publications">
							<h3 id="goldStandardPublicationReferences">
								<fontawesome:icon icon="link" />

								<c:out value=" "/>
								<fmt:message key="publications.related.referencedAndCited" />
							</h3>

							<ul class="nav nav-tabs">
								<c:if test="${not empty goldstandard.references}">
									<c:choose>
										<c:when test="${tabActive == false}">
											<c:set var="tabActive" value="true" />
											<c:set var="liClassReferences" value="active" />
										</c:when>
										<c:otherwise>
											<c:set var="liClassReferences" value="" />
										</c:otherwise>
									</c:choose>
									<li class="${liClassReferences}">
										<a data-toggle="tab" href="#related-publications-references">
											<fmt:message key="goldStandard.resource.info.references" />
										</a>
									</li>
								</c:if>

								<c:if test="${not empty goldstandard.referencedBy}">
									<c:choose>
										<c:when test="${tabActive == false}">
											<c:set var="tabActive" value="true" />
											<c:set var="liClassReferencedBy" value="active" />
										</c:when>
										<c:otherwise>
											<c:set var="liClassReferencedBy" value="" />
										</c:otherwise>
									</c:choose>
									<li class="${liClassReferencedBy}">
										<a data-toggle="tab" href="#related-publications-referencedy">
											<fmt:message key="goldStandard.resource.info.citations" />
										</a>
									</li>
								</c:if>

								<c:if test="${not empty goldstandard.subGoldStandards}">
									<c:choose>
										<c:when test="${tabActive == false}">
											<c:set var="tabActive" value="true" />
											<c:set var="liClassSubGoldStandards" value="active" />
										</c:when>
										<c:otherwise>
											<c:set var="liClassSubGoldStandards" value="" />
										</c:otherwise>
									</c:choose>
									<li class="${liClassSubGoldStandards}">
										<a data-toggle="tab" href="#related-publications-subgoldstandards">
											<fmt:message key="publications.related.incollection" />
										</a>
									</li>
								</c:if>
							</ul>

							<div class="tab-content">
								<div class="tab-pane ${liClassReferences}" id="related-publications-references">
									<ul data-items="3" class="more-list" data-inline="true">
										<c:forEach var="reference" items="${goldstandard.references}">
											<li>
												<c:if test="${isAdmin}">
													<button class="deleteRelation btn btn-danger btn-xs" data-relation="REFERENCE" data-interhash="${reference.interHash}">Delete</button>
												</c:if>
												<goldstandardpublication:simpleDetails publication="${reference}" />
											</li>
										</c:forEach>
									</ul>

									<c:if test="${isAdmin}">
										<a class="btn btn-xs btn-success add-reference-link" data-toggle="collapse" href="#admin-add-reference" role="button" aria-expanded="false" aria-controls="admin-add-reference">
											<fmt:message key="post.resource.addreference" />
										</a>
										<div class="collapse" id="admin-add-reference">
											<h4><fmt:message key="post.resource.addreference" /></h4>
											<input data-relation="REFERENCE" type="text" placeholder="add publication" class="addRelation form-control" />
										</div>
									</c:if>
								</div>

								<div class="tab-pane ${liClassReferencedBy}" id="related-publications-referencedy">
									<ul data-items="3" class="more-list" data-inline="true">
										<c:forEach var="reference" items="${goldstandard.referencedBy}">
											<li>
												<c:if test="${isAdmin}">
													<button class="deleteRelation btn btn-danger btn-xs" data-relation="REFERENCE" data-interhash="${reference.interHash}">Delete</button>
												</c:if>
												<goldstandardpublication:simpleDetails publication="${reference}" />
											</li>
										</c:forEach>
									</ul>
								</div>

								<div class="tab-pane ${liClassSubGoldStandards}" id="related-publications-subgoldstandards">
									<ul data-items="3" class="more-list" data-inline="true">
										<c:forEach var="reference" items="${goldstandard.subGoldStandards}">
											<li>
												<c:if test="${isAdmin}">
													<button class="deleteRelation btn btn-danger btn-xs" data-relation="REFERENCE" data-interhash="${reference.interHash}">Delete</button>
												</c:if>
												<goldstandardpublication:simpleDetails publication="${reference}" />
											</li>
										</c:forEach>
									</ul>
								</div>
							</div>

						</div>
					</c:if>


					<!-- Referenced and Referencing posts (Admin only) -->
					<c:if test="${not hasRelatedPublication and isAdmin}">
						<h3>
							<fontawesome:icon icon="link" />
							<c:out value=" "/>
							<fmt:message key="publications.related.referencedAndCited" />
						</h3>

						<a class="btn btn-xs btn-success add-reference-link" data-toggle="collapse" href="#admin-add-reference2" role="button" aria-expanded="false" aria-controls="admin-add-reference2">
							<fmt:message key="post.resource.addreference" />
						</a>
						<div class="collapse" id="admin-add-reference2">
							<h4><fmt:message key="post.resource.addreference" /></h4>
							<input data-relation="REFERENCE" type="text" placeholder="add publication" class="addRelation form-control" />
						</div>
					</c:if>


					<c:if test="${showFolkSonomyLinks}">
						<!--+
							| discussion
							+-->
						<c:set var="collapseClass" value="collapse" />
						<c:set var="collapseExpanded" value="false" />
						<c:if test="${not empty goldstandard.discussionItems}">
							<c:set var="collapseClass" value="collapse in" />
							<c:set var="collapseExpanded" value="true" />
						</c:if>
						<h3>
							<fontawesome:icon icon="comments" />
							<c:out value=" "/>
							<fmt:message key="publications.discussion.headline"/>

							<a class="discussion-toggle-link btn-primary badge" data-toggle="collapse" href="#discussion-section" role="button" aria-expanded="${collapseExpanded}" aria-controls="discussion-section">
								<fmt:message key="show" />
								/
								<fmt:message key="hide" />
							</a>
						</h3>

						<div class="${collapseClass}" id="discussion-section" aria-expanded="${collapseExpanded}">
							<!-- discussion -->
							<discussion:discussion resource="${goldstandard}" loginUser="${loginUser}"  postUserName="${postUserName}" postIsPublic="${postIsPublic}" />
						</div>
					</c:if>


				</div>

				<!-- right side -->
				<div class="col-md-3 hidden-xs hidden-sm sidebar">

					<!-- Quick cite -->
					<h3>
						<fontawesome:icon icon="quote-right" />
						<c:out value=" "/>
						<fmt:message key="publications.citations.headline" />
					</h3>

					<sidebar:quickcite post="${post}" isCommunityPost="true" />


					<!-- Search on... -->
					<h3>
						<fontawesome:icon icon="search" />
						<c:out value=" "/>
						<fmt:message key="resources.searchExternal.dropDownLabel" />
					</h3>
					<pub2:externalSearchPublication loggedinUser="${loginUser}" publication="${goldstandard}" buttonGroupClass="sidebar-links" />


					<c:if test="${isAdmin}">
						<h4>Published in</h4>

						<c:if test="${not empty publishedIn}">
							<goldstandardpublication:simpleDetails publication="${publishedIn}" />
						</c:if>
						<c:choose>
							<c:when test="${empty publishedIn}">
								<input data-relation="PART_OF" type="text" placeholder="published in ..." class="addRelation form-control" />
							</c:when>
							<c:otherwise>
								<button class="deleteRelation btn btn-danger btn-xs" data-relation="PART_OF" data-interhash="${publishedIn.interHash}">Delete Published In</button>
							</c:otherwise>
						</c:choose>
					</c:if>
				</div>

			</div>
		</div>
	</div>
</jsp:root>