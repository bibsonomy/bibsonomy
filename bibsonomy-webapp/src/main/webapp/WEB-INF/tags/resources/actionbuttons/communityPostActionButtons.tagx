<jsp:root version="2.0" 
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/resources/actionbuttons"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:bs="urn:jsptagdir:/WEB-INF/tags/layout/bootstrap"
>

	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true" />
	<jsp:directive.attribute name="resourceType" type="java.lang.String" required="true" />
	<jsp:directive.attribute name="communityPostExists" type="java.lang.Boolean" required="true" />
	<jsp:directive.attribute name="postOfLoggedInUser" type="org.bibsonomy.model.Post" required="true"/>
	<jsp:directive.attribute name="ckey" type="java.lang.String" required="false"/>

	<c:set var="buttonClasses" value="btn btn-default"/>
	<c:set var="loginUser" value="${command.context.loginUser}" />
	<c:set var="showApprovalOptions" value="${command.context.loginUser.hasGroupLevelPermission('COMMUNITY_POST_INSPECTION') or command.context.loginUser.role eq 'ADMIN'}" />


	<c:if test="${command.context.userLoggedIn}">
		<c:if test="${not loginUser.spammer}">

		<fmt:message var="additionalText" key="post.actions.edit"/>
		<c:choose>
			<c:when test="${not communityPostExists}">
				<buttons:copyPostButton
						userLoggedIn="${true}"
						post="${post}"
						resourceType="${resourceType}"
						buttonClasses="${buttonClasses}"
						titleKey="post.actions.edit.gold.title"
						forceCommunityCreate="${true}"
						icon="pencil"
						additionalText="${additionalText}"
				/>
			</c:when>
			<c:otherwise>
				<c:if test="${!post.approved or loginUser.hasGroupLevelPermission('COMMUNITY_POST_INSPECTION') or loginUser.role eq 'ADMIN'}">
					<buttons:editPostButton
							post="${post}"
							resourceType="${resourceType}"
							buttonClasses="${buttonClasses}"
							enforceCommunityPost="${true}"
							additionalText="${additionalText}"
					/>
				</c:if>
			</c:otherwise>
		</c:choose>
		</c:if>

		<c:if test="${resourceType eq 'bibtex'}">
			<c:if test="${showApprovalOptions and not post.approved}">
				<fmt:message var="additionalText" key="post.actions.edit.approve"/>
				<buttons:approvePostButton
						post="${post}"
						resourceType="${resourceType}"
						buttonClasses="${buttonClasses}"
						communityPostExists="${communityPostExists}"
						additionalText="${additionalText}"
						ckey="${ckey}"
				/>
			</c:if>
		</c:if>

		<c:if test="${communityPostExists}">
			<fmt:message var="additionalText" key="post.actions.history"/>
			<buttons:historyButton
					post="${post}"
					resourceType="${resourceType}"
					buttonClasses="${buttonClasses}"
					enforceCommunityPost="${true}"
					additionalText="${additionalText}"
			/>
		</c:if>

		<c:if test="${loginUser.role eq 'ADMIN' and communityPostExists}">
			<fmt:message var="additionalText" key="post.actions.delete"/>
			<buttons:deletePostButton
					userLoggedIn="${true}"
					post="${post}"
					resourceType="${resourceType}"
					loginUser="${loginUser}"
					buttonClasses="btn btn-default"
					additionalText="${additionalText}"
			/>
		</c:if>

		<c:if test="${empty postOfLoggedInUser}">
			<fmt:message var="additionalText" key="${resourceType}.actions.copy.title"/>

			<bs:linkButton
					href="${relativeUrlGenerator.getCopyUrlOfPost(post) }"
					size="${org.bibsonomy.webapp.view.constants.BootstrapButtonSize.defaultSize}"
					style="primary"
					icon="copy"
					textKey="post.actions.copy"
					titleKey="${resourceType}.actions.copy.title"
			/>
		</c:if>
	</c:if>

</jsp:root>