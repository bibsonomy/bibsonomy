<jsp:root version="2.0" 
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions">

	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true" />
	<jsp:directive.attribute name="loginUser" type="org.bibsonomy.model.User" required="true" />
	<jsp:directive.attribute name="userLoggedIn" type="java.lang.Boolean" required="true" />
	<jsp:directive.attribute name="resourceType" type="java.lang.String" required="true" />
	<jsp:directive.attribute name="buttonClasses" type="java.lang.String" required="true" />
	<jsp:directive.attribute name="additionalText" type="java.lang.String" required="false"/>
	
	<c:set var="loginUserName" value="${loginUser.name}" /> 
	
	<spring:eval expression="T(org.bibsonomy.model.factories.ResourceFactory).isCommunityResource(post.resource)" var="isCommunityPost" />
	
	<c:set var="isRemotePost" value="${not empty post.systemUrl and not mtl:isSameHost(post.systemUrl, properties['project.home'])}" />
	
	<c:set var="editableByUser" value="${loginUserName eq post.user.name}" />
	<!-- FIXME: extract this check; used more than once in the view! -->
	<c:forEach items="${loginUser.groups}" var="group">
		<c:if test="${group.name eq post.user.name }">
			<c:set var="ms" value="${group.getGroupMembershipForUser(loginUserName) }" />
			<c:if test="${not empty ms and (ms.groupRole eq 'MODERATOR' or ms.groupRole eq 'ADMINISTRATOR') }">
				<c:set var="editableByUser" value="${true}" />
			</c:if>
		</c:if>
	</c:forEach>
	
	<c:set var="disabled" value="${false}" />
	<c:choose>
		<!-- community post -->
		<c:when test="${isCommunityPost}">
			<c:if test="${loginUser.role ne 'ADMIN'}">
				<fmt:message key="${resourceType}.actions.delete.inactive" var="deleteTitle"/>
				<c:set var="disabled" value="${true}" />
			</c:if>
		</c:when>
		<c:when test="${systemReadOnly}">
			<fmt:message key="system.readOnly.info" var="deleteTitle" />
			<c:set var="disabled" value="${true}" />
		</c:when>
		<!-- general -->
		<c:when test="${not editableByUser and not isCommunityPost}">
			<fmt:message key="${resourceType}.actions.delete.inactive" var="deleteTitle"/>
			<c:set var="disabled" value="${true}" />
		</c:when>
		<c:otherwise>
			<fmt:message key="${resourceType}.actions.delete.title" var="deleteTitle"/>
		</c:otherwise>
	</c:choose>
	
	<c:set var="buttonClasses" value="${buttonClasses}${disabled ? ' disabled' : ''}" />
	<a class="${buttonClasses} confirmdelete" href="${relativeUrlGenerator.getDeleteUrlOfPost(post, command.context.ckey)}" title="${deleteTitle}" data-type="post">
		<span class="fa fa-times"><!--  --></span>
		<c:if test="${not empty additionalText}">
			<c:out value=" " />
			<span>${additionalText}</span>
		</c:if>
		<span class="sr-only"><fmt:message key="post.actions.delete"/></span>
	</a>
</jsp:root>
	