<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common"
	xmlns:bib="urn:jsptagdir:/WEB-INF/tags/resources/bibtex"
	xmlns:bibgs="urn:jsptagdir:/WEB-INF/tags/resources/goldstandardpublication"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:bs="urn:jsptagdir:/WEB-INF/tags/bs"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/resources/actionbuttons"
	>

	<jsp:directive.attribute name="listView" type="org.bibsonomy.webapp.command.ListCommand" required="true"/>
	<jsp:directive.attribute name="loginUserName" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="disableListNavigation" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="requPath" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="requQueryString" type="java.lang.String" required="false"/>
	<jsp:directive.attribute name="listIndex" type="java.lang.String" required="false" />
	<jsp:directive.attribute name="extTitle" type="java.lang.String" required="false" />
	<jsp:directive.attribute name="widthClassName"  type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="disableResourcetypeSelection" type="java.lang.Boolean" required="false"/>

	<jsp:directive.attribute name="hideCounts" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="hideMetaData" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="hidePostMenu" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="hidePostRating" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="headlineInBody" type="java.lang.Boolean" required="false" />

	<jsp:directive.attribute name="sortPage" type="java.lang.String" required="false" />
	<jsp:directive.attribute name="showSortInfoHeadline" type="java.lang.Boolean" required="false" />

	<jsp:directive.attribute name="onlyLinkAuthorsWithPersons" type="java.lang.Boolean" required="false" />

	<!-- list Headline options -->
	<jsp:directive.attribute name="hideHeadline" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="showPageOptions" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="showBookmark" type="java.lang.Boolean" required="true"/>
	<jsp:directive.attribute name="showPublication" type="java.lang.Boolean" required="true"/>
	<jsp:directive.attribute name="isClipboard" type="java.lang.Boolean" required="true" />

	<!-- fragments -->
	<jsp:directive.attribute name="addAsFirstItem" fragment="true" required="false" />
	<jsp:directive.attribute name="addAsLastItem" fragment="true" required="false" />
	<jsp:directive.attribute name="customPageOptions" fragment="true" required="false"/>
	<jsp:directive.attribute name="additionalSorting" fragment="true"/>

	<spring:eval var="interHashId" expression="T(org.bibsonomy.common.enums.HashID).INTER_HASH.id" />

	<c:if test="${empty listIndex}">
		<c:set var="listIndex" value="0"/>
	</c:if>

	<c:if test="${empty hideCounts}">
		<c:set var="hideCounts" value="${false}" />
	</c:if>

	<c:if test="${empty hideMetaData}">
		<c:set var="hideMetaData" value="${false}" />
	</c:if>

	<c:if test="${empty headlineInBody}">
		<c:set var="headlineInBody" value="${false}" />
	</c:if>

	<c:if test="${empty hidePostMenu}">
		<c:set var="hidePostMenu" value="${false}" />
	</c:if>

	<c:if test="${empty hidePostRating}">
		<c:set var="hidePostRating" value="${false}" />
	</c:if>

	<c:if test="${empty onlyLinkAuthorsWithPersons}">
		<c:set var="onlyLinkAuthorsWithPersons" value="${false}" />
	</c:if>

	<!-- FIXME: find a better way to disable the headline -->
	<rc:list
		titleKey="publications"
		loginUserName="${loginUserName}"
		listViewStartParamName="bibtex.start"
		otherPostsUrlPrefix="/bibtex/${interHashId}"
		listID="publications_${listIndex}"
		className="publicationsContainer"
		listView="${listView}"
		widthClassName="${widthClassName}"
		extTitle="${extTitle}"
		hideMetaData="${hideMetaData}"
		headlineInBody="${headlineInBody}"
		hideCounts="${hideCounts}"
		hidePostMenu="${hidePostMenu}"
		hidePostRating="${hidePostRating}"
		hideHeadline="${hideHeadline}"
		addAsFirstItem="${addAsFirstItem}"
		addAsLastItem="${addAsLastItem}">
		<jsp:attribute name="listHeadline">
			<h3 class="list-headline">
				<fmt:message key="publications" />
				&amp;nbsp;<small class="hidden-lg hidden-md">(<a id="hide-publications" href=""><fmt:message key="list.hide" /></a>)</small>
				<!-- Display total number of entries in list -->
				<c:if test="${not hideCounts and not empty listView.list and listView.totalCount != 0}">
					<c:set var="totalTitle"><fmt:message key="list.total" />: <c:out value=" ${listView.totalCount} "/> <fmt:message key="publications" /></c:set>
					<span class="badge" title="${totalTitle}" style="position:relative; top: -3px; left: 3px;">${listView.totalCount}</span>
					&amp;nbsp;
				</c:if>

				<c:if test="${showSortInfoHeadline}">
					<rc:sortInfoHeadline requPathAndQuery="/${requPath}${requQueryString}" resourceType="${listView.resourcetype}" useIndex="${listView.useIndex}" sortPage="${sortPage}"/>
				</c:if>
				
				<c:if test="${empty showPageOptions or showPageOptions}">
					<c:out value=" " />
					<c:choose>
						<c:when test="${not empty customPageOptions}">
							<jsp:invoke fragment="customPageOptions"/>
						</c:when>
						<c:otherwise>
							<div class="btn-group dropdown-align-right all-resources-menu">
								<rc:actions resourceType="bibtex"
									sortPage="${command.sortPage}"
									sortPageOrder="${command.sortPageOrder}"
									entriesPerPage="${listView.entriesPerPage}"
									showBookmark="${showBookmark}"
									showPublication="${showPublication}"
									isClipboard="${isClipboard}"
									disableListNavigation="${disableListNavigation}"
									disableResourcetypeSelection="${disableResourcetypeSelection}"
									additionalSorting="${additionalSorting}"/>
							</div>
						</c:otherwise>
					</c:choose>
				</c:if>
			</h3>
		</jsp:attribute>

		<jsp:attribute name="title">
			<c:choose>
				<c:when test="${empty post.user.name}">
					<bibgs:title post="${post}"/>
				</c:when>
				<c:otherwise>
					<bib:title post="${post}"/>
				</c:otherwise>
			</c:choose>
		</jsp:attribute>

		<!-- Bibtex description -->
		<jsp:attribute name="desc">
			<bib:desc publication="${post.resource}"
					  post="${post}"
					  restrictToFirstAuthors="${true}"
					  onlyLinkAuthorsWithPersons="${onlyLinkAuthorsWithPersons}"/>
		</jsp:attribute>

		<!-- BibTeX action -->
		<jsp:attribute name="actions">
			<buttons:actions loginUserName="${loginUserName}" post="${post}" disableResourceLinks="${false}" resourceType="bibtex"/>
		</jsp:attribute>

		<!-- BibTeX preview + document upload -->
		<jsp:attribute name="documents">
			<bib:documents post="${post}"/>
		</jsp:attribute>

	</rc:list>

</jsp:root>