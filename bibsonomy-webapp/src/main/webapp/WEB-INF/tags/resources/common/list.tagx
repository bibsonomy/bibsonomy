<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions">
	
	<jsp:directive.attribute name="titleKey" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="listView" type="org.bibsonomy.webapp.command.ListCommand" required="true"/>
	<jsp:directive.attribute name="loginUserName" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="otherPostsUrlPrefix" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="listID"  type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="className"  type="java.lang.String" required="false"/>
	<jsp:directive.attribute name="widthClassName"  type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="listViewStartParamName" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="extTitle" type="java.lang.String" required="false" />

	<jsp:directive.attribute name="hideHeadline" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="hideCounts" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="hideMetaData" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="hidePostMenu" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="hidePostRating" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="headlineInBody" type="java.lang.Boolean" required="false" />

	<jsp:directive.attribute name="title" fragment="true" required="true"/>
	<jsp:directive.attribute name="desc" fragment="true" required="true"/>
	<jsp:directive.attribute name="actions" fragment="true" required="true"/>
	<jsp:directive.attribute name="documents" fragment="true" required="true"/>
	<jsp:directive.attribute name="addAsFirstItem" fragment="true" required="false" />
	<jsp:directive.attribute name="addAsLastItem" fragment="true" required="false" />
	<jsp:directive.attribute name="listHeadline" fragment="true" required="false"/>

	<jsp:directive.variable name-given="post" scope="NESTED"/>

	<c:if test="${empty hideMetaData}">
		<c:set var="hideMetaData" value="${false}" />
	</c:if>

	<c:if test="${empty headlineInBody}">
		<c:set var="headlineInBody" value="${false}" />
	</c:if>

	<c:if test="${empty hideCounts}">
		<c:set var="hideCounts" value="${false}" />
	</c:if>

	<c:if test="${empty hidePostMenu}">
		<c:set var="hidePostMenu" value="${false}" />
	</c:if>

	<c:if test="${empty hidePostRating}">
		<c:set var="hidePostRating" value="${false}" />
	</c:if>

	<c:if test="${empty hideHeadline or not hideHeadline}">
		<jsp:invoke fragment="listHeadline" />
	</c:if>
	
	<div id="${listID}" class="${className} ${widthClassName}">
		<c:if test="${not empty extTitle}">
 			<div class="postlistTitle posts">${extTitle}</div>
		</c:if>
		
		<!-- iterate over a list of resources and create the list -->
		<ul class="posts ${widthClassName} media-list">
		
			<c:if test="${not empty addAsFirstItem}">
				<li class="first"><jsp:invoke fragment="addAsFirstItem"/></li>
			</c:if> 
			<c:choose>
				<c:when test="${not empty listView.list}">
					<c:forEach var="post" items="${listView.list}" varStatus="loopStatus">
						<!-- TODO: use css selector for this and remove -->
						<c:set var="additionalItemClasses" value="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}"/>
						
						<c:set var="resourcetype" value="${listView.resourcetype}" />
						<!-- show one post -->
						<rc:post post="${post}"
								 resourceType="${resourcetype}"
								 otherPostsUrlPrefix="${otherPostsUrlPrefix}"
								 loginUserName="${loginUserName}"
								 additionalItemClasses="${additionalItemClasses}"
								 hideMetaData="${hideMetaData}"
								 hidePostMenu="${hidePostMenu}"
								 hidePostRating="${hidePostRating}"
								 hideCounts="${hideCounts}"
								 headlineInBody="${headlineInBody}"
								 >
							<jsp:attribute name="title">    <jsp:invoke fragment="title"/>    </jsp:attribute>
							<jsp:attribute name="actions">  <jsp:invoke fragment="actions"/>  </jsp:attribute>
							<jsp:attribute name="desc">     <jsp:invoke fragment="desc"/>     </jsp:attribute>
							<jsp:attribute name="documents"><jsp:invoke fragment="documents"/></jsp:attribute>
						</rc:post>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<!-- only show message "no posts available" when no artificial list items (e.g. discussions) are present -->
					<c:if test="${empty addAsFirstItem}">
						<span class="post none">
							<fmt:message key="posts.no_matching_entries"/>
						</span>
					</c:if>
				</c:otherwise>
			</c:choose>
		</ul>
		<jsp:invoke fragment="addAsLastItem"/>
	</div>
</jsp:root>
