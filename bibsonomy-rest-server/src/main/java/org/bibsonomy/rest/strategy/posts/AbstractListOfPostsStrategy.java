/**
 * BibSonomy-Rest-Server - The REST-server.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.rest.strategy.posts;

import java.io.Writer;
import java.util.Date;
import java.util.List;

import org.bibsonomy.common.SortCriteria;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.QueryScope;
import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.SortOrder;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.factories.ResourceFactory;
import org.bibsonomy.rest.RESTConfig;
import org.bibsonomy.rest.RESTUtils;
import org.bibsonomy.rest.strategy.AbstractGetListStrategy;
import org.bibsonomy.rest.strategy.Context;
import org.bibsonomy.util.SortUtils;

/**
 * @author Jens Illig
 */
public abstract class AbstractListOfPostsStrategy extends AbstractGetListStrategy<List<? extends Post<? extends Resource>>> {
	protected final Class<? extends Resource> resourceType;
	protected final String hash;
	protected final GroupingEntity grouping;
	protected final String groupingValue;
	protected final String tagString;
	protected final List<String> tags;
	protected final String search;
	protected final QueryScope searchType;
	protected final List<SortCriteria> sortCriteria;
	protected final Date beforeChangeDate;
	protected final Date afterChangeDate;
	
	/**
	 * @param context
	 */
	public AbstractListOfPostsStrategy(final Context context) {
		super(context);
		this.tagString = context.getStringAttribute(RESTConfig.TAGS_PARAM, null);
		this.resourceType = ResourceFactory.getResourceClass(context.getStringAttribute(RESTConfig.RESOURCE_TYPE_PARAM, ResourceFactory.RESOURCE_CLASS_NAME));
		this.hash = context.getStringAttribute(RESTConfig.RESOURCE_PARAM, null);
		this.search = context.getStringAttribute(RESTConfig.SEARCH_PARAM, null);
		this.grouping = this.chooseGroupingEntity();
		this.tags = context.getTags(RESTConfig.TAGS_PARAM);
		final List<SortKey> sortKeys = SortUtils.parseSortKeys(context.getStringAttribute(RESTConfig.SORT_KEY_PARAM, null));
		final List<SortOrder> sortOrders = SortUtils.parseSortOrders(context.getStringAttribute(RESTConfig.SORT_ORDER_PARAM, null));
		this.sortCriteria = SortUtils.generateSortCriteria(sortKeys, sortOrders);
		this.beforeChangeDate = RESTUtils.getDateParam(context, RESTConfig.BEFORE_CHANGE_DATE_PARAM);
		this.afterChangeDate = RESTUtils.getDateParam(context, RESTConfig.AFTER_CHANGE_DATE_PARAM);

		String groupingValue;
		if (this.grouping != GroupingEntity.ALL) {
			groupingValue = context.getStringAttribute(this.grouping.toString().toLowerCase(), null);
			if (this.grouping == GroupingEntity.USER) {
				groupingValue = RESTUtils.normalizeUser(groupingValue, context);
			}
		} else {
			groupingValue = null;
		}
		this.groupingValue = groupingValue;

		// set search type
		this.searchType = context.getEnumAttribute(RESTConfig.SEARCH_TYPE_PARAM, QueryScope.class, QueryScope.LOCAL);
	}

	@Override
	protected void render(final Writer writer, final List<? extends Post<? extends Resource>> resultList) {
		this.getRenderer().serializePosts(writer, resultList, this.getView());
	}
	
	@Override
	protected final String getContentType() {
		return "posts";
	}
}