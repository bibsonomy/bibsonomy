/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Everything, which can be tagged, is derived from this class.
 * 
 * What may be accurate for representing the type of a Resource?
 * -> naturally its class! (which is lighter, more intuitive and flexible
 *    (eg reflective instantiation) and most notably more precise in
 *    type-safe generic methods than an enum).
 * 
 */
@Getter
@Setter
public abstract class Resource implements Serializable, Rateable {
	/**
	 * For persistence (Serializable) 
	 */
	private static final long serialVersionUID = -9153320764851332223L;
	
	/**
	 * How many posts with this resource exist.
	 */
	private int count;

	/**
	 * The inter user hash is less specific than the {@link #intraHash}.
	 */
	private String interHash;

	/**
	 * The intra user hash is relatively strict and takes many fields of this
	 * resource into account.
	 */
	private String intraHash;

	/**
	 * These are the {@link Post}s this resource belongs to.
	 */
	private List<Post<? extends Resource>> posts;

	/**
	 * Each resource has a title. 
	 * 
	 * TODO: It is given by the user and thus might better fit into the post.
	 */
	private String title;
	
	/**
	 * all comments for this resource
	 */
	private List<DiscussionItem> discussionItems;
	
	/**
	 * the rating (avg, …) of the resource
	 */
	private Double rating;
	
	/**
	 * number of all ratings for the resource
	 */
	private Integer numberOfRatings;
	
	/**
	 * FIXME: This method does not belong to the model!!!! It would be fine to
	 * make it a static method of this class and use the resource (to
	 * recalculate hashes for) as parameter.
	 */
	public abstract void recalculateHashes();


	/**
	 * @return posts
	 */
	public List<Post<? extends Resource>> getPosts() {
		if (this.posts == null) {
			this.posts = new LinkedList<Post<? extends Resource>>();
		}
		return this.posts;
	}

	@Override
	public String toString() {
		return "<" + intraHash + "/" + interHash + ">";
	}
}