/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model.util;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.Post;
import org.junit.Test;

/**
 * @author rja
 */
public class PostUtilsTest {
	
	@Test
	public void testGetOwnerFromKey() {
		final String owner = "test_test";
		final String hash = "121212";
		final String key = PostUtils.getKeyForPost(hash, owner);
		assertEquals(hash + "_" + owner, key);
		assertEquals(owner, PostUtils.getOwnerFromKey(key));
		assertEquals(hash, PostUtils.getHashFromKey(key));
	}

	@Test
	public void testSetGroupIdsPostOfQextendsResourceBoolean() {
		final Post<Bookmark> post = new Post<Bookmark>();
		final Set<Group> groups = new HashSet<Group>();
		
		final Group group1 = new Group(0);
		final Group group2 = new Group("kde");
		final Group group3 = new Group(7);
		
		groups.add(group1);
		groups.add(group2);
		groups.add(group3);
		post.setGroups(groups);
		
		/*
		 * set to non-spammer
		 */
		PostUtils.setGroupIds(post, false);
		assertEquals(0, group1.getGroupId());
		// FIXME: bug in UserUtils.getGroupId	
//		assertEquals(GroupID.INVALID.getId(), group2.getGroupId());
		assertEquals(7, group3.getGroupId());
		
		/*
		 * set to spammer
		 */
		PostUtils.setGroupIds(post, true);
		assertEquals(-2147483648, group1.getGroupId());
		// FIXME: bug in UserUtils.getGroupId
//		assertEquals(0, group2.getGroupId());
		assertEquals(-2147483641, group3.getGroupId());		

		/*
		 * set to spammer
		 */
		PostUtils.setGroupIds(post, true);
		assertEquals(-2147483648, group1.getGroupId());
		// FIXME: bug in UserUtils.getGroupId
//		assertEquals(0, group2.getGroupId());
		assertEquals(-2147483641, group3.getGroupId());		

		/*
		 * set to non-spammer
		 */
		PostUtils.setGroupIds(post, false);
		assertEquals(0, group1.getGroupId());
		// FIXME: bug in UserUtils.getGroupId	
//		assertEquals(GroupID.INVALID.getId(), group2.getGroupId());
		assertEquals(7, group3.getGroupId());
	}
}
