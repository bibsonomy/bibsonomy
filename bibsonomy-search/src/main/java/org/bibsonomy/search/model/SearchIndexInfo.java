/**
 * BibSonomy Search - Helper classes for search modules.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.search.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * infos about a search index
 *
 * @author ?
 */
@Getter
@Setter
public class SearchIndexInfo {
	/** The id of the index represented by this object */
	private String id;

	/** The current status of the index (active, inactive, generating, standby) */
	private SearchIndexStatus status;
	
	private SearchIndexState syncState;

	private SearchIndexStatistics statistics;

	private int writtenDocuments;
	private int allDocuments;

	public double getGenerationProgress() {
		if (this.allDocuments == 0) {
			// Catch dividing by zero
			return 0;
		}

		return (double) this.writtenDocuments / this.allDocuments;
	}

	public Date getGenerationEstimate() {
		if (this.allDocuments == 0 || this.writtenDocuments == 0) {
			// Catch dividing by zero and no Progress
			return null;
		}

		// Extract the timestamp from the id
		String[] parts = this.id.split("-");
		long startTime = Long.parseLong(parts[parts.length - 1]); // Last part is the timestamp

		long currentTime = System.currentTimeMillis();
		long timeElapsed = currentTime - startTime; // Time elapsed since start in milliseconds

		double timePerDocument = (double) timeElapsed / this.writtenDocuments;
		long timeRemaining = (long) (timePerDocument * (this.allDocuments - this.writtenDocuments));
		return new Date(currentTime + timeRemaining);
	}
}